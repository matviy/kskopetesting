package com.mockup.kscope.kscope.fragments.tabs;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ListView;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.adapters.LikesAdapter;
import com.mockup.kscope.kscope.adapters.MessagesAdapter;
import com.mockup.kscope.kscope.entity.ItemLike;
import com.mockup.kscope.kscope.entity.ItemMessage;
import com.mockup.kscope.kscope.fragments.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.InjectView;

/**
 * Created by Serhii_Slobodianiuk on 27.10.2015.
 */
public class FragmentLikes extends BaseFragment {

        @InjectView(R.id.list)
        ListView mList;

        private Context mContext;

        private List<ItemLike> likesList;
        private LikesAdapter mAdapter;

        @Nullable
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            mContext = getActivity();
        }

        @Override
        public void onViewCreated(View view, Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);

            likesList = new ArrayList<>();

            for (int i = 0; i < 10; i++){
                ItemLike like = new ItemLike();
                like.setImage(null);
                like.setUsername("User name");
                like.setLikes("like");
                like.setDate("4:54 PM");
                likesList.add(like);
            }

            mAdapter = new LikesAdapter(mContext, likesList);
            mList.setAdapter(mAdapter);
        }


    @Override
    public int getLayoutResource() {
        return R.layout.fragment_likes;
    }
}
