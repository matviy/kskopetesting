package com.mockup.kscope.kscope.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.adapters.viewHolder.SponsorsRecyclerViewHolder;
import com.mockup.kscope.kscope.entity.ItemSponsor;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Stafiiyevskyi on 28.10.2015.
 */
public class RecyclerSponsorsAdapter extends RecyclerView.Adapter<SponsorsRecyclerViewHolder> {

    private List<ItemSponsor> data;
    private Context context;
    OnClickCallback listener;

    public RecyclerSponsorsAdapter(OnClickCallback listener){
        this.listener = listener;
    }

    @Override
    public SponsorsRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sponsor, parent, false);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(v);
            }
        });

        SponsorsRecyclerViewHolder sponsorsRecyclerViewHolder = new SponsorsRecyclerViewHolder(view);

        return sponsorsRecyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(SponsorsRecyclerViewHolder holder, int position) {

        Picasso.with(context).load(Uri.parse(data.get(position).getLink_picture())).into(holder.sponsorBanner);
        holder.sponsorBanner.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,100));
        holder.sponsorDescription.setText(data.get(position).getTitle());

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(List<ItemSponsor> data, Context context){
        this.context = context;
        this.data = data;
    }

    public void onSponsorClick(int position){
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(data.get(position).getLink_homepage()));
        context.startActivity(browserIntent);
        data.get(position).getLink_homepage();
    }

    public interface OnClickCallback{
        void onItemClick(View v);
    }
}
