package com.mockup.kscope.kscope.api.request;

/**
 * Created by Stafiiyevskyi on 02.11.2015.
 */
public class LoginRequest extends Request{

    private Login request_param;

    public LoginRequest(String userLogin, String userPassword){
        setRequestCmd("user_login");
        request_param = new Login();
        request_param.user_login = userLogin;
        request_param.user_pas = userPassword;
    }

    private class Login {
        @SuppressWarnings("unused")
        private String user_login;
        @SuppressWarnings("unused")
        private String user_pas;
    }
}
