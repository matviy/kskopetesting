package com.mockup.kscope.kscope.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.api.SslRequestHandler;
import com.mockup.kscope.kscope.api.request.LoginRequest;
import com.mockup.kscope.kscope.api.response.LoginResponse;
import com.mockup.kscope.kscope.utils.PreferencesManager;
import com.mockup.kscope.kscope.utils.TypefaceManager;

import java.util.List;

import butterknife.InjectView;
import butterknife.OnClick;

public class SignInActivity extends BaseActivity implements Validator.ValidationListener {

    @NotEmpty
    @InjectView(R.id.username)
    EditText username;
    @NotEmpty
    @InjectView(R.id.password)
    EditText password;
    @InjectView(R.id.signin)
    Button signin;
    Validator validator;

    private PreferencesManager preferencesManager = PreferencesManager.getInstance();

    public static void launch(Context context) {
        Intent i = new Intent(context, SignInActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        validator = new Validator(this);
        validator.setValidationListener(this);
        setTypefaceElements();
    }

    private void setTypefaceElements() {
        username.setTypeface(TypefaceManager.obtainTypeface(this, TypefaceManager.RALEWAY_REGULAR));
        password.setTypeface(TypefaceManager.obtainTypeface(this, TypefaceManager.RALEWAY_REGULAR));
        signin.setTypeface(TypefaceManager.obtainTypeface(this, TypefaceManager.RALEWAY_BOLD));

        username.setText("userlogin1");
        password.setText("password");
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_signin;
    }

    @OnClick(R.id.signin)
    public void login() {
        validator.validate();
    }

    @Override
    public void onValidationSucceeded() {

        LoginRequest loginRequest = new LoginRequest(username.getText().toString(), password.getText().toString());
        SslRequestHandler.getInstance().sendRequest(loginRequest, LoginResponse.class, new SslRequestHandler.SslRequestListener<LoginResponse>() {

            @Override
            public void onStart() {

            }

            @Override
            public void onSuccess(LoginResponse response) {
                Log.i("Response login ", response.toString());
                if (Integer.parseInt(response.getRequestResult())!=0){
                    preferencesManager.setSessionId(response.getSessionId());
                    preferencesManager.setUserId(response.getIdUser());
                    preferencesManager.setUsername(response.getUserLogin());
                    MainActivity.launch(SignInActivity.this);
                }


            }

            @Override
            public void onFailure(String errorMsg) {

            }

            @Override
            public void onFinish() {

            }
        });

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }
}
