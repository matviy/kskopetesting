package com.mockup.kscope.kscope.entity;

import android.graphics.drawable.Drawable;

/**
 * Created by Serhii_Slobodianiuk on 28.10.2015.
 */
public class ItemGallery {

    private int photo;
    private String type;

    public int getPhoto() {
        return photo;
    }

    public void setPhoto(int photo) {
        this.photo = photo;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
