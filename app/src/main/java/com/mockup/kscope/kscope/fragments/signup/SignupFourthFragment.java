package com.mockup.kscope.kscope.fragments.signup;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.TextView;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.activities.SignupContainerActivity;
import com.mockup.kscope.kscope.customView.TypefaceTextView;
import com.mockup.kscope.kscope.fragments.BaseFragment;

import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by MPODOLSKY on 12.06.2015.
 */
public class SignupFourthFragment extends BaseFragment {

    @InjectView(R.id.description)
    EditText fullDescription;
    @InjectView(R.id.tag_line)
    EditText tagDescription;
    @InjectView(R.id.symbol_count)
    TypefaceTextView countSymbol;

    public static Fragment newInstance() {
        SignupFourthFragment fragment = new SignupFourthFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((SignupContainerActivity)getActivity()).setTitle(String.format(getString(R.string.signup_title_n), SignupContainerActivity.FOURTH_FRAGMENT_ID));
        fullDescription.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                countSymbol.setVisibility(View.VISIBLE);
                countSymbol.setText(fullDescription.getText().toString().length()+" / 256");
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                countSymbol.setText(fullDescription.getText().toString().length()+" / 256");
            }

            @Override
            public void afterTextChanged(Editable s) {
                countSymbol.setText(fullDescription.getText().toString().length()+" / 256");
            }
        });
    }

    @Override
    public int getLayoutResource() {
        return R.layout.fragment_signup_p4;
    }

    @OnClick(R.id.next)
    public void next(){
        ((SignupContainerActivity)getActivity()).setFragment(SignupContainerActivity.LAST_FRAGMENT_ID);
    }

    private void initTypefaceElements(){
        tagDescription.setTextColor(getResources().getColor(R.color.input_text_field_color));

    }
}
