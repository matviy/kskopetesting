package com.mockup.kscope.kscope.fragments.tabs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ProgressBar;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.adapters.TabsCommunitiesAdapter;
import com.mockup.kscope.kscope.api.SslRequestHandler;
import com.mockup.kscope.kscope.api.request.GetCommunityRequest;
import com.mockup.kscope.kscope.api.response.GetCommunityResponse;
import com.mockup.kscope.kscope.entity.ItemCommunities;
import com.mockup.kscope.kscope.fragments.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.InjectView;

/**
 * Created by Serhii_Slobodianiuk on 27.10.2015.
 */
public class FragmentTabsCommunities extends BaseFragment {

    @InjectView(R.id.list_communities)
    ExpandableListView lvCommunities;
    @InjectView(R.id.progress_bar)
    ProgressBar progressBar;

    private SslRequestHandler.SslRequestListener requestListener;
    private TabsCommunitiesAdapter adapter;
    @Nullable
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestListener = new SslRequestHandler.SslRequestListener<GetCommunityResponse>() {
            List<ItemCommunities> communities;

            @Override
            public void onStart() {
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onSuccess(GetCommunityResponse response) {
                communities = response.getCommunity();
            }

            @Override
            public void onFailure(String errorMsg) {

            }

            @Override
            public void onFinish() {
                if (progressBar != null) {
                    progressBar.setVisibility(View.GONE);
                    lvCommunities.setAdapter(adapter);
                    adapter.setItems(communities);
                }
            }
        };
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        adapter = new TabsCommunitiesAdapter(getActivity(), new ArrayList<ItemCommunities>());
        lvCommunities.setAdapter(adapter);
        lvCommunities.setGroupIndicator(null);

        GetCommunityRequest getCommunityRequest = new GetCommunityRequest();
        SslRequestHandler.getInstance().sendRequest(getCommunityRequest, GetCommunityResponse.class, requestListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onPause() {
        super.onPause();
        progressBar.setVisibility(View.GONE);
        SslRequestHandler.getInstance().cancelAsyncRequest(requestListener);
    }


    @Override
    public int getLayoutResource() {
        return R.layout.fragment_my_communities;
    }
}
