package com.mockup.kscope.kscope.customView;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.utils.DisplayUtil;
import com.mockup.kscope.kscope.utils.TypefaceManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;

/**
 * Created by a7med on 28/06/2015.
 */
public class CustomCalendarView extends LinearLayout implements CustomGestureListener.SwipeGestureCallBack{


    // how many days to show, defaults to six weeks, 42 days
    private static final int DAYS_COUNT = 35;

    // default date format
    private static final String DATE_FORMAT = "MMM yyyy";

    // date format
    private String dateFormat;

    // current displayed month
    private Calendar currentDate = Calendar.getInstance();

    //event handling
    private EventHandler eventHandler = null;
    private ControlHandler controlHandler = null;
    private CustomGestureListener gestureListener;

    // internal components
    private LinearLayout header;
    private ImageView btnPrev;
    private ImageView btnNext;
    private TypefaceTextView txtDate;
    private TypefaceTextView txtYear;
    private GridView grid;

//    // seasons' rainbow
//    int[] rainbow = new int[] {
//            R.color.summer,
//            R.color.fall,
//            R.color.winter,
//            R.color.spring
//    };

    // month-season association
    int[] monthSeason = new int[]{2, 2, 3, 3, 3, 0, 0, 0, 1, 1, 1, 2};

    public CustomCalendarView(Context context) {
        super(context);
    }

    public CustomCalendarView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initControl(context, attrs);
    }

    public CustomCalendarView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initControl(context, attrs);
    }

    /**
     * Load control xml layout
     */
    private void initControl(Context context, AttributeSet attrs) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.control_calendar, this);

        loadDateFormat(attrs);
        assignUiElements();
        assignClickHandlers();

        updateCalendar();
    }

    private void loadDateFormat(AttributeSet attrs) {
        TypedArray ta = getContext().obtainStyledAttributes(attrs, R.styleable.CustomCalendarView);

        try {
            // try to load provided date format, and fallback to default otherwise
            dateFormat = ta.getString(R.styleable.CustomCalendarView_dateFormat);
            if (dateFormat == null)
                dateFormat = DATE_FORMAT;
        } finally {
            ta.recycle();
        }
    }

    private void assignUiElements() {
        // layout is inflated, assign local variables to components
        header = (LinearLayout) findViewById(R.id.calendar_header);
        btnPrev = (ImageView) findViewById(R.id.calendar_prev_button);
        btnNext = (ImageView) findViewById(R.id.calendar_next_button);
        txtDate = (TypefaceTextView) findViewById(R.id.calendar_date_display);
        txtYear = (TypefaceTextView)findViewById(R.id.calendar_year_display);
        grid = (GridView) findViewById(R.id.calendar_grid);
    }

    private void assignClickHandlers() {
        // add one month and refresh UI
        btnNext.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                currentDate.add(Calendar.MONTH, 1);

                grid.setVisibility(INVISIBLE);
                if(controlHandler!=null){
                    controlHandler.nextMonthPressed();
                }
                animationNext();
            }
        });

        // subtract one month and refresh UI
        btnPrev.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                currentDate.add(Calendar.MONTH, -1);
                if (controlHandler != null) {
                    controlHandler.previousMonthPressed();
                }
                animationPrevios();
            }
        });

        // long-pressing a day
        grid.setOnItemLongClickListener(null);

        gestureListener = new CustomGestureListener(getContext());
        grid.setOnTouchListener(gestureListener);
        gestureListener.setSwipeGestureCallBack(this);
    }

    private void animationNext(){

        Animation slideLeftAnimation = AnimationUtils.loadAnimation(CustomCalendarView.this.getContext(), R.anim.left_slide);
        final Animation showAnimation = AnimationUtils.loadAnimation(CustomCalendarView.this.getContext(), R.anim.alpha_show);
        slideLeftAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                grid.startAnimation(showAnimation);
                updateCalendar();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        grid.startAnimation(slideLeftAnimation);
//                grid.startAnimation(slideRightAnimation);
        grid.setVisibility(View.VISIBLE);
    }

    private void animationPrevios(){
        final Animation slideRightAnimation = AnimationUtils.loadAnimation(CustomCalendarView.this.getContext(), R.anim.right_slide);
        final Animation showAnimation = AnimationUtils.loadAnimation(CustomCalendarView.this.getContext(), R.anim.alpha_show);
        slideRightAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                grid.startAnimation(showAnimation);
                updateCalendar();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        grid.startAnimation(slideRightAnimation);
//                grid.startAnimation(slideRightAnimation);

    }

    /**
     * Display dates correctly in grid
     */
    public void updateCalendar() {
        updateCalendar(null);
    }

    /**
     * Display dates correctly in grid
     */
    public void updateCalendar(HashSet<Date> events) {
        ArrayList<Date> cells = new ArrayList<>();
        Calendar calendar = (Calendar) currentDate.clone();

        // determine the cell for current month's beginning
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        int monthBeginningCell = calendar.get(Calendar.DAY_OF_WEEK) - 1;

        // move calendar backwards to the beginning of the week
        calendar.add(Calendar.DAY_OF_MONTH, -monthBeginningCell);

        // fill cells
        while (cells.size() < DAYS_COUNT) {
            cells.add(calendar.getTime());
            calendar.add(Calendar.DAY_OF_MONTH, 1);
        }

        // update grid
        grid.setAdapter(new CalendarAdapter(getContext(), cells, events));

        // update title
        SimpleDateFormat sdf = new SimpleDateFormat("MMMM");
        txtDate.setText(sdf.format(currentDate.getTime()).toUpperCase());
        sdf = new SimpleDateFormat("yyyy");
        txtYear.setText(sdf.format(currentDate.getTime()).toUpperCase());

        header.setBackgroundColor(getResources().getColor(R.color.white));
       /* // set header color according to current season
        int month = currentDate.get(Calendar.MONTH);
        int season = monthSeason[month];
        int color = rainbow[season];

        header.setBackgroundColor(getResources().getColor(color));*/
    }

    @Override
    public void onSwipe(CustomGestureListener.Direction direction) {

        switch (direction){
            case LEFT:
                currentDate.add(Calendar.MONTH, 1);
                animationNext();
                if(controlHandler!=null){
                    controlHandler.nextMonthPressed();
                }
                Log.i("Swipe ", " to left");
                break;
            case RIGHT:
                currentDate.add(Calendar.MONTH, -1);
                animationPrevios();
                if (controlHandler != null) {
                    controlHandler.previousMonthPressed();
                }
                Log.i("Swipe ", " to right");
                break;

        }
    }


    private class CalendarAdapter extends ArrayAdapter<Date> {
        // days with events
        private HashSet<Date> eventDays;


        // for view inflation
        private LayoutInflater inflater;

        public CalendarAdapter(Context context, ArrayList<Date> days, HashSet<Date> eventDays) {
            super(context, R.layout.control_calendar_day, days);
            this.eventDays = eventDays;
            inflater = LayoutInflater.from(context);
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {

            Date date = getItem(position);
            int day = date.getDate();
            int month = date.getMonth();
            int year = date.getYear();
            int widthCell = DisplayUtil.getScreenWidth(getContext())/7;

            Date today = new Date();


            if (view == null){
                view = inflater.inflate(R.layout.control_calendar_day, parent, false);
            }

            view.setLayoutParams(new AbsListView.LayoutParams(widthCell,widthCell));



            ((TypefaceTextView) view).setTextColor(getResources().getColor(R.color.calendar_current_date));

            if (month != today.getMonth() || year != today.getYear()) {

                ((TypefaceTextView) view).setTextColor(getResources().getColor(R.color.calendar_not_current_date));
            } else if (day == today.getDate()) {
                view.setBackgroundResource(R.drawable.bg_calendar_selected);
                ((TypefaceTextView) view).setTypeface(TypefaceManager.obtainTypeface(getContext(), TypefaceManager.RALEWAY_BOLD));
                ((TypefaceTextView) view).setTextColor(getResources().getColor(R.color.today));
                ((TypefaceTextView) view).setTextSize(getResources().getDimension(R.dimen.calendar_today_date_size));
            }


            ((TypefaceTextView) view).setText(String.valueOf(date.getDate()));

            return view;
        }
    }

    /**
     * Assign event handler to be passed needed events
     */
    public void setEventHandler(EventHandler eventHandler) {
        this.eventHandler = eventHandler;
    }

    public void setControlHandler(ControlHandler controlHandler){
        this.controlHandler = controlHandler;
    }

    /**
     * This interface defines what events to be reported to
     * the outside world
     */
    public interface EventHandler {
        void onDayLongPress(Date date);
    }

    public interface ControlHandler {
        void nextMonthPressed();
        void previousMonthPressed();
    }
}