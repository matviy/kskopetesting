package com.mockup.kscope.kscope.entity;

/**
 * Created by Alexander Rubanskiy on 12.06.2015.
 */

public class ItemMainMenu {

    private String itemName;

    public ItemMainMenu(String itemName) {
        this.itemName = itemName;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }
}
