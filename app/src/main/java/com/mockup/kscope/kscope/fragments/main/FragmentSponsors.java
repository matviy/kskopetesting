package com.mockup.kscope.kscope.fragments.main;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.adapters.RecyclerSponsorsAdapter;
import com.mockup.kscope.kscope.api.SslRequestHandler;
import com.mockup.kscope.kscope.api.request.GetSponsorRequest;
import com.mockup.kscope.kscope.api.response.GetSponsorResponse;
import com.mockup.kscope.kscope.entity.ItemSponsor;
import com.mockup.kscope.kscope.fragments.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.InjectView;

/**
 * Created by Stafiiyevskyi on 28.10.2015.
 */
public class FragmentSponsors extends BaseFragment implements RecyclerSponsorsAdapter.OnClickCallback {
    @InjectView(R.id.recycler_sponsors)
    RecyclerView sponsorsRecycler;
    @InjectView(R.id.progress)
    ProgressBar progressBar;
    @InjectView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    SslRequestHandler.SslRequestListener requestListener;
    private RecyclerSponsorsAdapter recyclerSponsorsAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        List<ItemSponsor> list = new ArrayList<>();
        initAdapter(list);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        sponsorsRecycler.setLayoutManager(gridLayoutManager);
        prepareRecyclerView();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                getSponsors();
            }
        });

        requestListener = new SslRequestHandler.SslRequestListener<GetSponsorResponse>() {
            List<ItemSponsor> sponsorsResult = null;

            @Override
            public void onStart() {
                if (!swipeRefreshLayout.isRefreshing()) {
                    progressBar.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onSuccess(GetSponsorResponse response) {
                ArrayList<ItemSponsor> sponsors = response.getRequestParam().getSponsor();

                for (ItemSponsor item : sponsors) {
                    Log.i("item sponsor: ", item.getTitle());
                }
                sponsorsResult = sponsors;
                Log.i("Sponsor response ", response.toString());

            }

            @Override
            public void onFailure(String errorMsg) {

            }

            @Override
            public void onFinish() {
                if (swipeRefreshLayout != null) {
                    swipeRefreshLayout.setRefreshing(false);
                    progressBar.setVisibility(View.INVISIBLE);
                    initAdapter(sponsorsResult);
                    prepareRecyclerView();
                }

            }
        };

        getSponsors();

        return view;
    }

    @Override
    public int getLayoutResource() {
        return R.layout.fragment_sponsors;
    }


    private void initAdapter(List<ItemSponsor> sponsors) {
        recyclerSponsorsAdapter = new RecyclerSponsorsAdapter(this);
        recyclerSponsorsAdapter.setData(sponsors, getActivity());
    }


    private void prepareRecyclerView() {
        sponsorsRecycler.setAdapter(recyclerSponsorsAdapter);
//        sponsorsRecycler.addItemDecoration(new CalendarItemDecoration(getActivity()));
    }

    private void getSponsors() {
        GetSponsorRequest getSponsorRequest = new GetSponsorRequest();
        SslRequestHandler.getInstance().sendRequest(getSponsorRequest, GetSponsorResponse.class, requestListener);

    }

    @Override
    public void onItemClick(View v) {
        int position = sponsorsRecycler.getChildAdapterPosition(v);
        recyclerSponsorsAdapter.onSponsorClick(position);
    }

    @Override
    public void onPause() {
        super.onPause();
        SslRequestHandler.getInstance().cancelAsyncRequest(requestListener);
    }
}
