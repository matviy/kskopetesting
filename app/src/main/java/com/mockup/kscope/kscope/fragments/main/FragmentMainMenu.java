package com.mockup.kscope.kscope.fragments.main;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.activities.CommunitySectionsActivity;
import com.mockup.kscope.kscope.activities.MainActivity;
import com.mockup.kscope.kscope.activities.StartUpActivity;
import com.mockup.kscope.kscope.adapters.MainMenuListAdapter;
import com.mockup.kscope.kscope.dialogs.ConfirmDialog;
import com.mockup.kscope.kscope.dialogs.DialogCallback;
import com.mockup.kscope.kscope.entity.ItemMainMenu;
import com.mockup.kscope.kscope.fragments.BaseFragment;
import com.mockup.kscope.kscope.utils.PreferencesManager;

import java.util.ArrayList;

import butterknife.InjectView;

/**
 * Created by Alexander Rubanskiy on 12.06.2015.
 */

public class FragmentMainMenu extends BaseFragment {

    @InjectView(R.id.main_menu_list)
    ListView lvMainMenu;


    private String[] ArrayMainList;

    private String title;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ArrayMainList = getActivity().getResources().getStringArray(R.array.main_menu_array);
        MainMenuListAdapter adapter = new MainMenuListAdapter(getActivity(), initList());
        adapter.setCallback(new MainMenuListAdapter.Callback() {
            @Override
            public void onMenuClick(int position) {
                addFragment(position);
            }
        });
        lvMainMenu.setAdapter(adapter);

    }

    private ArrayList<ItemMainMenu> initList() {
        ArrayList<ItemMainMenu> items = new ArrayList<>();
        for (int i = 0; i < ArrayMainList.length; i++) {
            ItemMainMenu itemMainMenu = new ItemMainMenu(ArrayMainList[i]);
            items.add(itemMainMenu);
        }
        return items;
    }

    private void addFragment(int position) {
        Fragment fragment = getFragment(position);
        if (fragment != null) {
            ((MainActivity) getActivity()).addFragment(fragment, title);
        }

    }


    private Fragment getFragment(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = FragmentProfile.newInstance();
                title = "My profile";
                break;
            case 1:
                fragment = new FragmentCommunities();
                title = "Communities";
                break;
            case 2:
                fragment = new FragmentCalendar();
                title = "Calendar of events";
                break;
            case 3:
                fragment = new FragmentAbout();
                title = "About us";
                break;
            case 4:
                fragment = new FragmentSponsors();
                title = "Sponsors";
                break;

            case 5:
                fragment = new FragmentTestimonials();
                title = "Testimonials";
                break;
            case 6:
                CommunitySectionsActivity.launch(getActivity());
                return null;
            case 7:
                ConfirmDialog dialog = ConfirmDialog.newInstance(R.string.dialog_logout_title,
                        R.string.dialog_logout_messages,
                        R.string.dialog_confirm,
                        R.string.dialog_cancel);
                dialog.setCallback(new DialogCallback.EmptyCallback() {
                    @Override
                    public void confirm() {
                        logout();
                    }
                });
                dialog.show(getFragmentManager(), "dialog");
                break;
            default:
                break;
        }
        return fragment;
    }

    private void logout() {
        PreferenceManager.getDefaultSharedPreferences(getActivity()).edit().clear().apply();
        PreferencesManager.getInstance().clear();

        // facebook logout
        //LoginManager.getInstance().logOut();

        Intent intent = new Intent(getActivity(), StartUpActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

    @Override
    public int getLayoutResource() {
        return R.layout.fragment_main;
    }

}