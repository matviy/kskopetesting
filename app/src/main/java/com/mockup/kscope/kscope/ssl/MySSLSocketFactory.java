package com.mockup.kscope.kscope.ssl;

import org.apache.http.conn.ssl.SSLSocketFactory;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class MySSLSocketFactory extends SSLSocketFactory {
    SSLContext sslContext = SSLContext.getInstance("TLS");

    public MySSLSocketFactory(KeyStore truststore) throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException, UnrecoverableKeyException {
        super(truststore);

        TrustManager tm = new X509TrustManager() {
            public void checkClientTrusted(X509Certificate[] chain, String authType) {
            }

            public void checkServerTrusted(X509Certificate[] chain, String authType) {
            }

            public X509Certificate[] getAcceptedIssuers() {
                return null;
            }
        };

        sslContext.init(null, new TrustManager[]{tm}, null);
    }

    public MySSLSocketFactory(SSLContext context) throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException, UnrecoverableKeyException {
        super(null);
        sslContext = context;
    }

    @Override
    public Socket createSocket(Socket socket, String host, int port, boolean autoClose) throws IOException, UnknownHostException {
//        ((SSLSocket) socket).setEnabledProtocols(new String[]{"TLSv1.2", "TLSv1"});
//        ((SSLSocket) socket).setEnabledCipherSuites(new String[]{"ECDHE-RSA-RC4-SHA", "RC4-SHA"});
//        return sslContext.getSocketFactory().createSocket(socket, host, port, autoClose);
        SSLSocket s = (SSLSocket)sslContext.getSocketFactory().createSocket(socket, host, port, autoClose);
        s.setEnabledProtocols(new String[] {"TLSv1"} );
        return s;
    }

    public Socket createSocket(String host, int port) throws IOException, UnknownHostException {
        SSLSocket s = (SSLSocket)sslContext.getSocketFactory().createSocket(host, port);
        String[] cipherSuites = s.getSSLParameters().getCipherSuites();
        ArrayList<String> cipherSuiteList = new ArrayList<>(Arrays.asList(cipherSuites));

        cipherSuiteList.add("DES-CBC-SHA");
        cipherSuites = cipherSuiteList.toArray(new String[cipherSuiteList.size()]);
        s.getSSLParameters().setCipherSuites(cipherSuites);

        String[] protocols = s.getSSLParameters().getProtocols();
        ArrayList<String> protocolList = new ArrayList<>(Arrays.asList(protocols));

        for (int ii = protocolList.size() - 1; ii >= 0; --ii )
        {
            if ((protocolList.get(ii).contains("SSLv3")) || (protocolList.get(ii).contains("TLSv1.1")) || (protocolList.get(ii).contains("TLSv1.2")))
                protocolList.remove(ii);
        }

        protocols = protocolList.toArray(new String[protocolList.size()]);
        s.setEnabledProtocols(protocols);

        getHostnameVerifier().verify(host, s);
//        s.setEnabledProtocols(new String[] {"TLSv1.2"} );
//        s.setSSLParameters();
        return s;
//        return sslContext.getSocketFactory().createSocket(host, port);
    }

    @Override
    public Socket createSocket() throws IOException {
        return sslContext.getSocketFactory().createSocket();
    }
}
