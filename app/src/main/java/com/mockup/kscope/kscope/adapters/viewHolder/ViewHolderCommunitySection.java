package com.mockup.kscope.kscope.adapters.viewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mockup.kscope.kscope.R;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Stafiiyevskyi on 29.10.2015.
 */
public class ViewHolderCommunitySection extends RecyclerView.ViewHolder {
    @InjectView(R.id.community_sections_image)
    public ImageView bannerCommunity;
    @InjectView(R.id.community_sections_name)
    public TextView nameCommunity;

    public ViewHolderCommunitySection(View itemView) {
        super(itemView);
        ButterKnife.inject(this, itemView);
    }

}
