package com.mockup.kscope.kscope.api.response;

import android.widget.RelativeLayout;

import com.mockup.kscope.kscope.entity.ItemFriend;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Serhii_Slobodianiuk on 02.11.2015.
 */
public class FriendsResponse extends Response {
    private FriendsRequestParam request_param = new FriendsRequestParam();

//    public ArrayList<RelationShipFriends> getFriends() {
//        return request_param.relationship;
//    }
    public ArrayList<String> getFriends() {
        return request_param.getRelationship().getUser_login();
    }

    public FriendsRequestParam getRequestParam() {
        return request_param;
    }

    public static class FriendsRequestParam extends InvalidParam {

        private String data_count;
        private String data_limit;
        private String data_offset;

        private Relationship relationship = new Relationship();

//
        public String getDataCount() {
            return data_count;
        }

        public String getDataLimit() {
            return data_limit;
        }

        public String getDataOffset() {
            return data_offset;
        }

        public Relationship getRelationship() {
            return relationship;
        }
    }

    public static class  Relationship extends InvalidParam{
        private ArrayList<String> user_login;

        public ArrayList<String> getUser_login() {
            return user_login;
        }
    }
}
