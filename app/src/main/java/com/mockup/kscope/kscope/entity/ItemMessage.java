package com.mockup.kscope.kscope.entity;

/**
 * Created by Serhii_Slobodianiuk on 27.10.2015.
 */
public class ItemMessage {
    private String image;
    private String sender;
    private String message;
    private String date;
    private boolean isOnline;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public boolean isOnline() {
        return isOnline;
    }

    public void setOnline(boolean isOnline) {
        this.isOnline = isOnline;
    }
}
