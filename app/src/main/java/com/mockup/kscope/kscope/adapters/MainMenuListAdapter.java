package com.mockup.kscope.kscope.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.entity.ItemMainMenu;

import java.util.ArrayList;

/**
 * Created by Alexander Rubanskiy on 12.06.2015.
 */

public class MainMenuListAdapter extends ArrayAdapter<ItemMainMenu> {

    private final static int RESOURCE = R.layout.item_main_list;

    private final Context context;
    private LayoutInflater inflater;
    private ArrayList<ItemMainMenu> items;
    private Callback callback;

    public MainMenuListAdapter(Context context, ArrayList<ItemMainMenu> items) {
        super(context, RESOURCE, items);
        this.context = context;
        this.items = items;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public ItemMainMenu getItem(int position) {
        return items.get(position);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        View item = convertView;

        if (item == null) {

            item = inflater.inflate(RESOURCE, parent, false);
            holder = new ViewHolder();
            holder.tvName = (TextView) item.findViewById(R.id.tv_item_name);
            item.setTag(holder);
        } else {
            holder = (ViewHolder) item.getTag();
        }

        final ItemMainMenu menuItem = getItem(position);

        holder.tvName.setText(menuItem.getItemName());

        item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (callback != null) {
                    callback.onMenuClick(position);
                }
            }
        });

        return item;
    }

    static class ViewHolder {
        private TextView tvName;
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    public interface Callback{
        void onMenuClick(int position);
    }
}
