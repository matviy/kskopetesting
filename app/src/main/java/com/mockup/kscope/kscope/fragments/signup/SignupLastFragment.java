package com.mockup.kscope.kscope.fragments.signup;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Button;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.activities.SignupContainerActivity;
import com.mockup.kscope.kscope.fragments.BaseFragment;
import com.mockup.kscope.kscope.utils.TypefaceManager;

import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by MPODOLSKY on 12.06.2015.
 */
public class SignupLastFragment extends BaseFragment {

    @InjectView(R.id.save_and_close)
    Button saveClose;
    @InjectView(R.id.save_and_login)
    Button saveLogin;

    public static Fragment newInstance() {
        SignupLastFragment fragment = new SignupLastFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((SignupContainerActivity)getActivity()).setTitle(getString(R.string.signup_title_complete));
        saveClose.setTypeface(TypefaceManager.obtainTypeface(getActivity(), TypefaceManager.RALEWAY_BOLD));
        saveLogin.setTypeface(TypefaceManager.obtainTypeface(getActivity(),TypefaceManager.RALEWAY_BOLD));
    }

    @Override
    public int getLayoutResource() {
        return R.layout.fragment_signup_complete;
    }

    @OnClick(R.id.save_and_close)
    public void saveAndClose(){
        ((SignupContainerActivity)getActivity()).saveAndClose();
    }

    @OnClick(R.id.save_and_login)
    public void saveAndLogin(){
        ((SignupContainerActivity)getActivity()).saveAndLogin();
    }
}
