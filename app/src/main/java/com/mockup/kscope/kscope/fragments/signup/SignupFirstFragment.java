package com.mockup.kscope.kscope.fragments.signup;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Order;
import com.mobsandgeeks.saripaar.annotation.Password;
import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.activities.SignupContainerActivity;
import com.mockup.kscope.kscope.fragments.BaseFragment;
import com.mockup.kscope.kscope.utils.TypefaceManager;

import java.util.List;


import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by MPODOLSKY on 12.06.2015.
 */
public class SignupFirstFragment extends BaseFragment implements com.mobsandgeeks.saripaar.Validator.ValidationListener{

    @NotEmpty
    @Order(1)
    @InjectView(R.id.username)
    public EditText username;

    @NotEmpty
    @Order(2)
    @InjectView(R.id.confirm_username)
    public EditText usernameConfirm;

    @NotEmpty
    @Password(min = 4)
    @Order(3)
    @InjectView(R.id.password)
    public EditText password;

    @NotEmpty
    @Order(4)
    @ConfirmPassword
    @InjectView(R.id.confirm_password)
    public EditText passwordConfirm;

    private Validator validator;

    public static Fragment newInstance() {
        final SignupFirstFragment fragment = new SignupFirstFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        Bundle args = getArguments();
        validator = new Validator(this);
        validator.setValidationListener(this);
        setTypefaceElements();
        ((SignupContainerActivity) getActivity()).setTitle(String.format(getString(R.string.signup_title_n), SignupContainerActivity.FIRST_FRAGMENT_ID));
    }

    @Override
    public int getLayoutResource() {
        return R.layout.fragment_signup_p1;
    }

    private void setTypefaceElements(){
        username.setTypeface(TypefaceManager.obtainTypeface(getActivity(),TypefaceManager.RALEWAY_REGULAR));
        usernameConfirm.setTypeface(TypefaceManager.obtainTypeface(getActivity(),TypefaceManager.RALEWAY_REGULAR));
        password.setTypeface(TypefaceManager.obtainTypeface(getActivity(),TypefaceManager.RALEWAY_REGULAR));
        passwordConfirm.setTypeface(TypefaceManager.obtainTypeface(getActivity(),TypefaceManager.RALEWAY_REGULAR));
    }

    @OnClick(R.id.next)
    public void next() {
        validator.validate();
    }

    @Override
    public void onValidationSucceeded() {
        if (confirmField(getActivity(),username,usernameConfirm,"username")){
            ((SignupContainerActivity) getActivity()).setFragment(SignupContainerActivity.SECOND_FRAGMENT_ID);
        }

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this.getActivity());

            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this.getActivity(), message, Toast.LENGTH_LONG).show();
            }
        }
    }

    public boolean confirmField(Context context, EditText field, EditText fieldConfirm, String fieldName) {
        String fieldString = field.getText().toString();
        String fieldConfirmString = fieldConfirm.getText().toString();
        String fieldNameString = fieldName;

        if (!TextUtils.equals(fieldString, fieldConfirmString)) {

            Toast.makeText(context, this.getString(R.string.incorrect_confirm) + fieldNameString + ".", Toast.LENGTH_LONG).show();
            return false;

        } else return true;

    }
}
