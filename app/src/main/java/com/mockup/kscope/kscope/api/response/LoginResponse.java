package com.mockup.kscope.kscope.api.response;

/**
 * Created by Stafiiyevskyi on 02.11.2015.
 */
public class LoginResponse extends Response {

    private LoginRequestParam request_param = new LoginRequestParam();

    public String getIdUser() {
        return request_param.id_user;
    }

    public String getIdUserRole() {
        return request_param.id_user_role;
    }

    public String getUserLogin() {
        return request_param.user_login;
    }

    public LoginRequestParam getRequestParam() {
        return request_param;
    }

    public static class LoginRequestParam extends InvalidParam {

        private String id_user;
        private String id_user_role;
        private String user_login;

        public String getIdUser() {
            return id_user;
        }

        public String getIdUserRole() {
            return id_user_role;
        }

        public String getUserLogin() {
            return user_login;
        }
    }

}