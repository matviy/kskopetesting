package com.mockup.kscope.kscope.fragments.community_sections;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.activities.CommunitySectionsActivity;
import com.mockup.kscope.kscope.adapters.CommunitySubSectionAdapter;
import com.mockup.kscope.kscope.entity.ItemCommunities;
import com.mockup.kscope.kscope.fragments.BaseFragment;

import java.util.ArrayList;

import butterknife.InjectView;

/**
 * Created by Stafiiyevskyi on 30.10.2015.
 */
public class FragmentCommunitySubSection extends BaseFragment implements AdapterView.OnItemClickListener {
    @InjectView(R.id.communities_list)
    ListView lvCommunities;
    private CommunitySubSectionAdapter adapter;
    private String title;

    private String[] ArrayCommunities = {"Community Subsection - First", "Community Subsection - Second",
            "Community Subsection - Third", "Community Subsection - Four"};

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        title = getArguments().getString(CommunitySectionsActivity.TITLE_KEY);
        ((CommunitySectionsActivity)getActivity()).setActionBarTitle(title, true);
        adapter = new CommunitySubSectionAdapter(getActivity(), initList());
        lvCommunities.setAdapter(adapter);
        lvCommunities.setOnItemClickListener(this);

    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        ((CommunitySectionsActivity)getActivity()).setActionBarTitle(title, true);
    }

    private ArrayList<ItemCommunities> initList() {
        ArrayList<ItemCommunities> items = new ArrayList<>();
        for (int i = 0; i < ArrayCommunities.length; i++) {
            ItemCommunities itemMainMenu = new ItemCommunities(ArrayCommunities[i]);
            items.add(itemMainMenu);
        }
        return items;
    }


    @Override
    public int getLayoutResource() {
        return R.layout.fragment_communities;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Fragment fragment = new FragmentCommunityNextSubSection();
        Bundle bundle = new Bundle();
        bundle.putString(CommunitySectionsActivity.TITLE_KEY,((ItemCommunities) parent.getItemAtPosition(position)).getName());
        fragment.setArguments(bundle);
        ((CommunitySectionsActivity) getActivity()).addFragment(fragment);
    }

}
