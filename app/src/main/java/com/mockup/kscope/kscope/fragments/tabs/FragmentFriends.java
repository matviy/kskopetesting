package com.mockup.kscope.kscope.fragments.tabs;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.adapters.FriendsAdapter;
import com.mockup.kscope.kscope.api.SslRequestHandler;
import com.mockup.kscope.kscope.api.request.FriendsRequest;
import com.mockup.kscope.kscope.api.response.FriendsResponse;
import com.mockup.kscope.kscope.entity.ItemFriend;
import com.mockup.kscope.kscope.fragments.BaseFragment;
import com.mockup.kscope.kscope.utils.PreferencesManager;

import java.util.ArrayList;

import butterknife.InjectView;

/**
 * Created by Serhii_Slobodianiuk on 27.10.2015.
 */
public class FragmentFriends extends BaseFragment {

    @InjectView(R.id.list)
    ListView mList;
    @InjectView(R.id.progress_bar)
    ProgressBar mProgressBar;

    private Context mContext;

    private PreferencesManager preferenceManager = PreferencesManager.getInstance();

    private ArrayList<ItemFriend> friendList;
    private FriendsAdapter mAdapter;
    private SslRequestHandler.SslRequestListener<FriendsResponse> requestListener;

    @Nullable
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        requestListener = new SslRequestHandler.SslRequestListener<FriendsResponse>() {
            @Override
            public void onStart() {
                if (mProgressBar != null) {
                    mProgressBar.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onSuccess(FriendsResponse response) {
                friendList = new ArrayList<>();
                ArrayList<String> friends = response.getFriends();
                for (int i = 0; i < friends.size(); i++) {
                    ItemFriend friend = new ItemFriend();
                    friend.setUser_login(friends.get(i));
                    friendList.add(friend);
                }
            }

            @Override
            public void onFailure(String errorMsg) {

            }

            @Override
            public void onFinish() {
                if (mProgressBar != null) {

                    mProgressBar.setVisibility(View.GONE);
                    mAdapter.setItems(friendList);
                }
            }
        };
    }

    @Override
    public void onStop() {
        super.onStop();
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mProgressBar.setVisibility(View.VISIBLE);

        mAdapter = new FriendsAdapter(mContext, new ArrayList<ItemFriend>());
        mList.setAdapter(mAdapter);

        final FriendsRequest friendsRequest = new FriendsRequest(preferenceManager.getSessionId());
        SslRequestHandler.getInstance().sendRequest(friendsRequest, FriendsResponse.class, requestListener);
    }

    @Override
    public int getLayoutResource() {
        return R.layout.fragment_friends;
    }

    @Override
    public void onPause() {
        super.onPause();
        SslRequestHandler.getInstance().cancelAsyncRequest(requestListener);
    }
}
