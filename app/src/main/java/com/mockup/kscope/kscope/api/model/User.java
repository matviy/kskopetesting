package com.mockup.kscope.kscope.api.model;

/**
 * Created by Serhii_Slobodianiuk on 03.11.2015.
 */
public class User {
    private String city;
    private String date_joined;
    private String first_name;
    private String id_user;
    private String mobile_phone;
    private String second_name;
    private String state;
    private String street_address1;
    private String street_address2;
    private String user_email;
    private String user_login;
    private String user_profile;
    private String work_phone;
    private String zip_code;

    public User() {
    }

    public User(String first_name, String second_name, String user_profile, String street_address1) {
        this.first_name = first_name;
        this.second_name = second_name;
        this.user_profile = user_profile;
        this.street_address1 = street_address1;
    }

    public String getCity() {
        return city;
    }

    public String getDate_joined() {
        return date_joined;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getId_user() {
        return id_user;
    }

    public String getMobile_phone() {
        return mobile_phone;
    }

    public String getSecond_name() {
        return second_name;
    }

    public String getState() {
        return state;
    }

    public String getStreet_address1() {
        return street_address1;
    }

    public String getStreet_address2() {
        return street_address2;
    }

    public String getUser_email() {
        return user_email;
    }

    public String getUser_login() {
        return user_login;
    }

    public String getUser_profile() {
        return user_profile;
    }

    public String getWork_phone() {
        return work_phone;
    }

    public String getZip_code() {
        return zip_code;
    }
}
