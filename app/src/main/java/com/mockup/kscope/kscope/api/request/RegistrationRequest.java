package com.mockup.kscope.kscope.api.request;

/**
 * Created by Alexander Rubanskiy on 30.10.2015.
 */

public class RegistrationRequest extends Request {

    private Registration request_param;

    public RegistrationRequest(String userLogin, String userPassword) {
        setRequestCmd("user_register");
        request_param = new Registration();
        request_param.user_login = userLogin;
        request_param.user_pas = userPassword;
    }

    private class Registration {
        @SuppressWarnings("unused")
        private String user_login;
        @SuppressWarnings("unused")
        private String user_pas;
        @SuppressWarnings("unused")
        private String id_user_role = "1";
    }

}
