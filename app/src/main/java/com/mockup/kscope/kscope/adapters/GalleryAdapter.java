package com.mockup.kscope.kscope.adapters;

import android.app.ActivityManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.entity.ItemGallery;
import com.squareup.picasso.LruCache;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Serhii_Slobodianiuk on 28.10.2015.
 **/
public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.ViewHolder> {

    public static final int CACHE_IMAGE_COUNT = 120;
    public static final int LAST_POSITION = -1;
    Picasso picasso;
    private List<ItemGallery> data;
    //    private List<Point> sizes;
    private RelativeLayout.LayoutParams mImageParams;
    private RelativeLayout.LayoutParams mDefaultParams;
    private Point mCurrentRange = new Point(0, 0);
    private Context context;
    private LruCache picassoCache;
    private Callback mCallback;
    private TopSquareTransformation mTopSquareTransformation;

    public GalleryAdapter(Context context, List<ItemGallery> data) {
        this.context = context;
        this.data = data;
        mDefaultParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                250);
        mImageParams = new RelativeLayout.LayoutParams(0, 0);
        int memClass = ((ActivityManager) context
                .getSystemService(Context.ACTIVITY_SERVICE))
                .getLargeMemoryClass();
        int cacheSize = 1024 * 1024 * memClass / 5;
        picassoCache = new LruCache(cacheSize);
        picasso = new Picasso.Builder(context).memoryCache(picassoCache).build();
        mTopSquareTransformation = new TopSquareTransformation();
    }

    public void setCallback(Callback callback) {
        mCallback = callback;
    }

    public void addItems(List<ItemGallery> newItems) {
        if (newItems != null) {
            data.addAll(newItems);
            notifyItemRangeInserted(getItemCount(), newItems.size());
        }
    }

    public List<ItemGallery> getItems() {
        return data;
    }

    public void setItems(List<ItemGallery> newItems) {
        data.clear();
        if (newItems != null) {
            data.addAll(newItems);
            notifyDataSetChanged();
        } else {
            notifyDataSetChanged();
        }
    }

    public ItemGallery getFirstItem() {
        return (data != null && data.size() > 0) ? data.get(0) : null;
    }

//    public List<ItemGallery> getFirstItems() {
//        return data.size() <= FragmentSearchResults.ITEM_LOAD_COUNT ? data : data.subList(0, FragmentSearchResults.ITEM_LOAD_COUNT - 1);
//    }

//    public void updateItemsWith(List<Asset> assets) {
//        if (data != null && assets != null) {
//            data.addAll(0, assets);
//            notifyDataSetChanged();
////            notifyItemRangeInserted(0, assets.size());
//        }
//    }

    public void remove(int position) {
        if (position == LAST_POSITION && getItemCount() > 0)
            position = getItemCount() - 1;

        if (position > LAST_POSITION && position < getItemCount()) {
            data.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void scroll(int first, int last) {
        if (first < 0) {
            first = 0;
        }
        if (first < mCurrentRange.x) {
            if (mCurrentRange.y - first > CACHE_IMAGE_COUNT) {
                clearCache(first + CACHE_IMAGE_COUNT, mCurrentRange.y);
                mCurrentRange.y = first + CACHE_IMAGE_COUNT;
            }
            mCurrentRange.x = first;
        } else if (last > mCurrentRange.y) {
            if (last - mCurrentRange.x > CACHE_IMAGE_COUNT) {
                clearCache(mCurrentRange.x, last - CACHE_IMAGE_COUNT);
                mCurrentRange.x = last - CACHE_IMAGE_COUNT;
            }
            mCurrentRange.y = last;
        }
    }

    public void clearCache(int start, int end) {
        int size = data.size();
        for (int i = start; i <= end; i++) {
            if (size >= end) {
                // String uri = UrlUtil.getThumbnailUrl(data.get(i));
                int uri = data.get(i).getPhoto();
                if (uri != 0) {
                    picasso.invalidate(String.valueOf(uri));
                }
            }
        }
    }

    public void invalidateCache() {
        picassoCache.clear();
    }

    public void clear() {
        data.clear();
        picassoCache.clear();
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_gallery_grid, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final ItemGallery item = data.get(position);
        String assetType = item.getType();
        holder.image.setTag(R.string.tag, item);
        picasso.cancelRequest(holder.image);
        holder.image.setImageResource(android.R.color.transparent);
        holder.play.setImageResource(android.R.color.transparent);
        if (!TextUtils.isEmpty(assetType) && assetType.equals("videos")) {
            holder.play.setImageResource(R.drawable.ic_gallery_video);
        } else {
            holder.play.setImageResource(R.drawable.ic_gallery);
        }
        //String url = UrlUtil.getThumbnailUrl(item);
        //holder.title.setText(metadata.getTitle());
        //holder.published.setText(String.valueOf(metadata.getPublicationDate()));
//        if (url != null) {
//            picasso
//                    .load(url)
//                    .noPlaceholder()
//                    .networkPolicy(NetworkPolicy.NO_STORE)
//                    .transform(mTopSquareTransformation)
//                    .into(holder.image);
//        }

        picasso
                .load(item.getPhoto()) /** url **/
                .noPlaceholder()
                .networkPolicy(NetworkPolicy.NO_STORE)
                .transform(mTopSquareTransformation)
                .into(holder.image);

        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Asset item = (Asset) holder.image.getTag(R.string.tag);
//                mCallback.onClick(Asset.create(item.serialize()));
            }
        });
        //DisplayUtil.setItemSize(holder.image.getWidth());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public interface Callback {
        void onClick(ItemGallery asset);
    }

//    @Override
//    public long getItemId(int position) {
//        return position;
//    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @InjectView(R.id.title)
        TextView title;
        @InjectView(R.id.image)
        ImageView image;
        @InjectView(R.id.published)
        TextView published;
        @InjectView(R.id.play)
        ImageView play;
        @InjectView(R.id.root)
        RelativeLayout mRoot;
        @InjectView(R.id.thumbnail_container)
        RelativeLayout mImageContainer;


        public ViewHolder(View view) {
            super(view);
            ButterKnife.inject(this, view);
        }
    }

    public class TopSquareTransformation implements Transformation {
        @Override
        public Bitmap transform(Bitmap source) {
            int width = source.getWidth();
            int height = source.getHeight();
            int size = Math.min(width, height);
            Bitmap result;
            if (width > height) {
                result = Bitmap.createBitmap(source, (width - height) / 2, 0, size, size);
            } else {
                result = Bitmap.createBitmap(source, 0, 0, size, size);
            }
            if (result != source) {
                source.recycle();
            }
            return result;
        }

        @Override
        public String key() {
            return "topsquare()";
        }
    }

}


