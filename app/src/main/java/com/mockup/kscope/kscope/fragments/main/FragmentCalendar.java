package com.mockup.kscope.kscope.fragments.main;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;


import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.adapters.RecyclerCalendarAdapter;
import com.mockup.kscope.kscope.customView.CalendarItemDecoration;
import com.mockup.kscope.kscope.customView.CustomCalendarView;
import com.mockup.kscope.kscope.entity.ItemEvent;
import com.mockup.kscope.kscope.fragments.BaseFragment;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import butterknife.InjectView;

/**
 * Created by Alexander Rubanskiy on 12.06.2015.
 */

public class FragmentCalendar extends BaseFragment implements CustomCalendarView.ControlHandler, CustomCalendarView.EventHandler{

    @InjectView(R.id.recycler_calendar)
    RecyclerView calendarRecycler;
    @InjectView(R.id.calendarview)
    CustomCalendarView calendarView;
    private RecyclerCalendarAdapter recyclerCalendarAdapter;

    private GregorianCalendar currentDate = (GregorianCalendar)Calendar.getInstance();

    @Override
    public void onResume() {
        super.onResume();
        initComponents();
        setDataToAdapterRecycler();
    }

    @Override
    public int getLayoutResource() {
        return R.layout.fragment_calendar;
    }

    private void initComponents(){
        calendarView.setControlHandler(this);
        calendarView.setEventHandler(this);
    }

    private void setDataToAdapterRecycler(){
        recyclerCalendarAdapter = new RecyclerCalendarAdapter(getActivity());
        ArrayList<ItemEvent> events = new ArrayList<>();
        GregorianCalendar calendar = (GregorianCalendar) currentDate.clone();



        for (int i = 0; i< 20; i++){
            ItemEvent event = new ItemEvent();
            calendar.set(Calendar.DAY_OF_MONTH,(i+1));
            event.setDateOfEvent(calendar.getTime());
            event.setEventDescription("Lorem ipsum dolor sit amet, consecte Maecenas gravida facilisis neque");
            event.setTypeOfPosition(RecyclerCalendarAdapter.NORMAL_POSITION);
            events.add(event);
        }

        recyclerCalendarAdapter.setData(events);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        calendarRecycler.addItemDecoration(new CalendarItemDecoration(getActivity()));
        calendarRecycler.setLayoutManager(layoutManager);
        calendarRecycler.setAdapter(recyclerCalendarAdapter);
    }

    @Override
    public void nextMonthPressed() {

    }

    @Override
    public void previousMonthPressed() {

    }

    @Override
    public void onDayLongPress(Date date) {

    }
}