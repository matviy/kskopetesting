package com.mockup.kscope.kscope.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.entity.ItemMessage;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Serhii_Slobodianiuk on 27.10.2015.
 */
public class MessagesAdapter extends BaseAdapter {
    private List<ItemMessage> messagesList;
    private Context mContext;

    public MessagesAdapter(Context mContext, List<ItemMessage> messagesList) {
        this.messagesList = messagesList;
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        return messagesList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder holder;
        if (convertView == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.item_messages, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        Picasso.with(mContext)
                .load(messagesList.get(position).getImage())
                .placeholder(R.drawable.bg_calendar_selected)
                .into(holder.image);

        holder.tvSenderName.setText(messagesList.get(position).getSender());
        holder.tvMessage.setText(messagesList.get(position).getMessage());
        holder.tvDate.setText(messagesList.get(position).getDate());

        if (messagesList.get(position).isOnline()) {
            holder.status.setImageResource(R.drawable.ic_message_on);
        } else {
            holder.status.setImageResource(R.drawable.ic_message_off);
        }

        return view;
    }

    static class ViewHolder {

        @InjectView(R.id.username)
        TextView tvSenderName;
        @InjectView(R.id.message)
        TextView tvMessage;
        @InjectView(R.id.image)
        ImageView image;
        @InjectView(R.id.status)
        ImageView status;
        @InjectView(R.id.date)
        TextView tvDate;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }


}
