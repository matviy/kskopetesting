package com.mockup.kscope.kscope.adapters.viewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.mockup.kscope.kscope.R;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Stafiiyevskyi on 27.10.2015.
 */
public class CalendarRecyclerViewHolder extends RecyclerView.ViewHolder{

    @InjectView(R.id.day)
    public TextView day;
    @InjectView(R.id.name_day_time)
    public TextView nameDayTime;
    @InjectView(R.id.description_event)
    public TextView descriptionEvent;
    @InjectView(R.id.swipe_calendar_item_layout)
    public SwipeLayout swipeLayout;
    @InjectView(R.id.delete_event_button)
    public ImageView cancel;
    @InjectView(R.id.confirm_event_button)
    public ImageView check;
    @InjectView(R.id.time_event_button)
    public ImageView time;
    @InjectView(R.id.menu_event_button)
    public ImageView list;
    @InjectView(R.id.top_item_view)
    public LinearLayout topLinearLayout;

    public CalendarRecyclerViewHolder(View itemView) {
        super(itemView);
        ButterKnife.inject(this, itemView);
    }

    public void bindDayText(String dayText){
        day.setText(dayText);
    }

    public void bindNameDayTime(String nameDayTimeText){
        nameDayTime.setText(nameDayTimeText);
    }

    public void bindDescriptionEvent(String descriptionEventText){
        descriptionEvent.setText(descriptionEventText);
    }
}
