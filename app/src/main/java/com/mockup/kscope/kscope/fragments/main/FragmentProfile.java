package com.mockup.kscope.kscope.fragments.main;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.activities.MainActivity;
import com.mockup.kscope.kscope.adapters.ViewPagerAdapter;
import com.mockup.kscope.kscope.api.SslRequestHandler;
import com.mockup.kscope.kscope.api.request.UserDataRequest;
import com.mockup.kscope.kscope.api.response.Response;
import com.mockup.kscope.kscope.api.response.UserDataResponse;
import com.mockup.kscope.kscope.fragments.BaseFragment;
import com.mockup.kscope.kscope.fragments.tabs.FragmentFriends;
import com.mockup.kscope.kscope.fragments.tabs.FragmentGallery;
import com.mockup.kscope.kscope.fragments.tabs.FragmentLikes;
import com.mockup.kscope.kscope.fragments.tabs.FragmentMessages;
import com.mockup.kscope.kscope.fragments.tabs.FragmentTabsCommunities;
import com.mockup.kscope.kscope.utils.PreferencesManager;

import butterknife.InjectView;

/**
 * Created by Alexander Rubanskiy on 12.06.2015.
 */

@SuppressWarnings("deprecation")
public class FragmentProfile extends BaseFragment {

    @InjectView(R.id.tabs)
    TabLayout mTabLayout;
    @InjectView(R.id.pager)
    ViewPager mViewPager;
    @InjectView(R.id.photo)
    ImageView photo;
    @InjectView(R.id.username)
    TextView username;
    @InjectView(R.id.working)
    TextView about;
    @InjectView(R.id.city)
    TextView address;
    @InjectView(R.id.progress_bar)
    ProgressBar progressBar;

    private Context mContext;
    private Resources mResources;

    private ImageView editProfile;
    private ImageView search;
    private PreferencesManager preferenceManager = PreferencesManager.getInstance();

    public static Fragment newInstance() {
        FragmentProfile fragmentProfile = new FragmentProfile();
        Bundle args = new Bundle();
        fragmentProfile.setArguments(args);
        return fragmentProfile;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        mResources = mContext.getResources();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
       // progressBar.setVisibility(View.VISIBLE);

        UserDataRequest userDataRequest = new UserDataRequest(preferenceManager.getSessionId());
        SslRequestHandler.getInstance().sendRequest(userDataRequest, UserDataResponse.class, new SslRequestHandler.SslRequestListener<UserDataResponse>() {
            @Override
            public void onStart() {
            }

            @Override
            public void onSuccess(UserDataResponse response) {
                //Log.e("User data: ", response.getUserData().toString());
                if(response.getUserData().getFirst_name() == null) {
                    username.setText(response.getUserData().getUser_login());
                }else {
                    username.setText(response.getUserData().getFirst_name() + " " + response.getUserData().getSecond_name());
                    about.setText(response.getUserData().getUser_profile());
                    address.setText(response.getUserData().getStreet_address1());
                }
            }

            @Override
            public void onFailure(String errorMsg) {

            }

            @Override
            public void onFinish() {
               // progressBar.setVisibility(View.GONE);
            }
        });

        setupToolbarMenu();
        setupViewPager(mViewPager);
        mTabLayout.setupWithViewPager(mViewPager);

        setupCustomTab(mResources.getDrawable(R.drawable.tab_friends_selector),
                "68", mResources.getString(R.string.my_friends), 0);
        setupCustomTab(mResources.getDrawable(R.drawable.tab_messages_selector),
                "79", mResources.getString(R.string.my_messages), 1);
        setupCustomTab(mResources.getDrawable(R.drawable.tab_gallery_selector),
                "9", mResources.getString(R.string.my_gallery), 2);
        setupCustomTab(mResources.getDrawable(R.drawable.tab_likes_selector),
                "39", mResources.getString(R.string.my_likes), 3);
        setupCustomTab(mResources.getDrawable(R.drawable.tab_communities_selector),
                "7", mResources.getString(R.string.my_communities), 4);

    }

    private void setupToolbarMenu() {
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        search = (ImageView) toolbar.findViewById(R.id.menu_search);
        showMenuItem(search);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        editProfile = (ImageView) toolbar.findViewById(R.id.menu_edit_profile);
        showMenuItem(editProfile);
        editProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = FragmentEditProfile.newInstance();
                if (fragment != null) {
                    hideMenuItem(editProfile, search);
                    ((MainActivity) getActivity()).addFragment(fragment, mResources.getString(R.string.edit_profile_title));
                }
            }
        });

    }

    private void setupCustomTab(Drawable icon, String count, String title, final int index) {
        final LinearLayout tabView = (LinearLayout) inflateCustomTabLayout();
        ImageView iconView = (ImageView) tabView.findViewById(R.id.icon_tab);
        iconView.setImageDrawable(icon);

        TextView countView = (TextView) tabView.findViewById(R.id.count_tab);
        countView.setText(count.toString());

        TextView titleView = (TextView) tabView.findViewById(R.id.title_tab);
        titleView.setText(title.toString());


        if (index == 0) {
            tabView.setSelected(true);
        }
        mTabLayout.getTabAt(index).setCustomView(tabView);

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFrag(new FragmentFriends(), mResources.getString(R.string.my_friends));
        adapter.addFrag(new FragmentMessages(), mResources.getString(R.string.my_messages));
        adapter.addFrag(new FragmentGallery(), mResources.getString(R.string.my_gallery));
        adapter.addFrag(new FragmentLikes(), mResources.getString(R.string.my_likes));
        adapter.addFrag(new FragmentTabsCommunities(), mResources.getString(R.string.my_communities));
        viewPager.setAdapter(adapter);
    }

    private void showMenuItem(ImageView menuView) {
        menuView.setVisibility(View.VISIBLE);
    }


    private void hideMenuItem(ImageView menuSearch, ImageView menuEdit) {
        menuSearch.setVisibility(View.GONE);
        menuEdit.setVisibility(View.GONE);
    }

    @Override
    public int getLayoutResource() {
        return R.layout.fragment_profile;
    }

    @Override
    public void onPause() {
        super.onPause();
        hideMenuItem(search, editProfile);
    }

    private View inflateCustomTabLayout() {
        return LayoutInflater.from(mContext).inflate(R.layout.layout_custom_tab, null);
    }


}