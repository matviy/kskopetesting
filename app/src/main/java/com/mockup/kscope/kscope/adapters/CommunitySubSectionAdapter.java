package com.mockup.kscope.kscope.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.entity.ItemCommunities;

import java.util.ArrayList;

/**
 * Created by Stafiiyevskyi on 30.10.2015.
 */
public class CommunitySubSectionAdapter extends ArrayAdapter<ItemCommunities> {

    private final static int RESOURCE = R.layout.item_community_list;

    private final Context context;
    private LayoutInflater inflater;
    private ArrayList<ItemCommunities> items;

    public CommunitySubSectionAdapter(Context context, ArrayList<ItemCommunities> items) {
        super(context, RESOURCE, items);
        this.context = context;
        this.items = items;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public ItemCommunities getItem(int position) {
        return items.get(position);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        View item = convertView;

        if (item == null) {

            item = inflater.inflate(RESOURCE, parent, false);
            holder = new ViewHolder();
            holder.tvName = (TextView) item.findViewById(R.id.tv_community_name);
            item.setTag(holder);
        } else {
            holder = (ViewHolder) item.getTag();
        }

        final ItemCommunities menuItem = getItem(position);

        holder.tvName.setText(menuItem.getName());


        return item;
    }

    static class ViewHolder {
        private TextView tvName;
    }

}