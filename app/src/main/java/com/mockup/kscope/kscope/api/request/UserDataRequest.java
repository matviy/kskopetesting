package com.mockup.kscope.kscope.api.request;

public class UserDataRequest extends Request {


    public UserDataRequest(String sessionId) {
        setRequestCmd("get_user_data");
        setSessionId(sessionId);
    }
}
