package com.mockup.kscope.kscope.utils;

import android.content.Context;
import android.content.SharedPreferences;


public class PreferencesManager {

    private static final String PREF_NAME = "settings";

    private static final String TOKEN = "token";
    private static final String TOKEN_TYPE = "token_type";
    private static final String SESSION_ID = "session";
    private static final String USERNAME = "username";
    private static final String USER_ID = "user_id";
    private static PreferencesManager sInstance;
    private final SharedPreferences mPref;


    private PreferencesManager(Context context) {
        mPref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public static synchronized void initializeInstance(Context context) {
        if (sInstance == null) {
            sInstance = new PreferencesManager(context);
        }
    }

    public static synchronized PreferencesManager getInstance() {
        if (sInstance == null) {
            throw new IllegalStateException(PreferencesManager.class.getSimpleName() +
                    " is not initialized, call initializeInstance(..) method first.");
        }
        return sInstance;
    }

    public SharedPreferences getPreferences() {
        return mPref;
    }

    public void remove(String key) {
        mPref.edit()
                .remove(key)
                .commit();
    }

    public boolean clear() {
        return mPref.edit()
                .clear()
                .commit();
    }

    public String getSessionId() {
        return mPref.getString(SESSION_ID, null);
    }

    public void setSessionId(String sessionId) {
        mPref.edit()
                .putString(SESSION_ID, sessionId)
                .commit();
    }

    public String getUsername() {
        return mPref.getString(USERNAME, null);
    }

    public void setUsername(String username) {
        mPref.edit()
                .putString(USERNAME, username)
                .commit();
    }

    public String getUserId() {
        return mPref.getString(USER_ID, null);
    }

    public void setUserId(String userId) {
        mPref.edit()
                .putString(USERNAME, userId)
                .commit();
    }
}
