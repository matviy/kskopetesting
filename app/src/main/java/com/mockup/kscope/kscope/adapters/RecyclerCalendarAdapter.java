package com.mockup.kscope.kscope.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.daimajia.swipe.SwipeLayout;
import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.adapters.viewHolder.CalendarRecyclerViewHolder;
import com.mockup.kscope.kscope.entity.ItemEvent;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by Stafiiyevskyi on 27.10.2015.
 */
public class RecyclerCalendarAdapter extends RecyclerView.Adapter<CalendarRecyclerViewHolder> {

    public static final int NORMAL_POSITION = 0;

    private Activity context;
    private List<ItemEvent> data;

    public RecyclerCalendarAdapter(Activity context) {
        this.context = context;
    }

    @Override
    public CalendarRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == NORMAL_POSITION) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recycler_calendar_swipe_lay, parent, false);
            CalendarRecyclerViewHolder recyclerViewHolder = new CalendarRecyclerViewHolder(view);
            return recyclerViewHolder;
        } else {

            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recycler_calendar_swipe_lay, parent, false);

            CalendarRecyclerViewHolder recyclerViewHolder = new CalendarRecyclerViewHolder(view);
            return recyclerViewHolder;
        }

    }


    @Override
    public void onBindViewHolder(final CalendarRecyclerViewHolder holder, final int position) {

        SimpleDateFormat sdf = new SimpleDateFormat("d");
        holder.bindDayText(sdf.format(data.get(position).getDateOfEvent().getTime()));
        SimpleDateFormat sdfNameDay = new SimpleDateFormat("EEEE h:mm ");
        SimpleDateFormat sdfNamePmAm = new SimpleDateFormat("a");
        holder.bindNameDayTime(sdfNameDay.format(data.get(position).getDateOfEvent().getTime())
                +(sdfNamePmAm.format(data.get(position).getDateOfEvent().getTime())).toLowerCase());
        holder.bindDescriptionEvent(data.get(position).getEventDescription());
        holder.swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);
        holder.topLinearLayout.setBackgroundColor(context.getResources().getColor(R.color.more_light_gray));
        holder.swipeLayout.addSwipeListener(new SwipeLayout.SwipeListener() {
            @Override
            public void onClose(SwipeLayout layout) {
                //when the SurfaceView totally cover the BottomView.
                layout.getChildAt(1).setBackgroundColor(context.getResources().getColor(R.color.more_light_gray));

            }

            @Override
            public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {
                //you are swiping.

            }

            @Override
            public void onStartOpen(SwipeLayout layout) {
                layout.getChildAt(1).setBackgroundColor(context.getResources().getColor(R.color.white));
            }

            @Override
            public void onOpen(SwipeLayout layout) {
                //when the BottomView totally show.
                layout.getChildAt(1).setBackgroundColor(context.getResources().getColor(R.color.white));

            }

            @Override
            public void onStartClose(SwipeLayout layout) {
                layout.getChildAt(1).setBackgroundColor(context.getResources().getColor(R.color.more_light_gray));
            }

            @Override
            public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {
                //when user's hand released.
            }
        });
        holder.cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                data.remove(holder.getAdapterPosition());
                RecyclerCalendarAdapter.this.notifyItemRemoved(holder.getAdapterPosition());
            }
        });

    }

    @Override
    public int getItemCount() {

        return data.size();

    }

    @Override
    public int getItemViewType(int position) {
        return data.get(position).getTypeOfPosition();
    }



    public void setData(List<ItemEvent> data) {
        this.data = data;
    }
}
