package com.mockup.kscope.kscope.fragments.community_sections;

import android.os.Bundle;
import android.view.View;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.activities.CommunitySectionsActivity;
import com.mockup.kscope.kscope.fragments.BaseFragment;

/**
 * Created by Stafiiyevskyi on 30.10.2015.
 */
public class FragmentCommunitySectionItem extends BaseFragment{
    private String title;

    @Override
    public int getLayoutResource() {
        return R.layout.fragment_item_section_place;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        title = getArguments().getString(CommunitySectionsActivity.TITLE_KEY);
        ((CommunitySectionsActivity)getActivity()).setActionBarTitle(title, true);
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        ((CommunitySectionsActivity)getActivity()).setActionBarTitle(title, true);
    }
}
