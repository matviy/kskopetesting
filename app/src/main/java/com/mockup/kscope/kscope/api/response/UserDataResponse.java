package com.mockup.kscope.kscope.api.response;

import com.mockup.kscope.kscope.api.model.User;

/**
 * Created by Serhii_Slobodianiuk on 03.11.2015.
 */
public class UserDataResponse extends Response {

    private UserRequestParam request_param = new UserRequestParam();

    public User getUserData(){
        return request_param.getUser_data();
    }

    public UserRequestParam getRequestParam(){
        return request_param;
    }

    public static class UserRequestParam extends InvalidParam {
        private User user_data;

        public User getUser_data() {
            return user_data;
        }
    }
}
