package com.mockup.kscope.kscope.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.fragments.community_sections.FragmentCommunitySectionRecycler;

/**
 * Created by Stafiiyevskyi on 29.10.2015.
 */
public class CommunitySectionsActivity extends BaseActivity {

    public static final String TITLE_KEY = "title_key";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Fragment fragment = new FragmentCommunitySectionRecycler();
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.fragmants_container, fragment, "tag")
                .commit();
        setActionBarTitle("Community Sections",true);
    }

    public void addFragment(Fragment fragment){
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.fragmants_container, fragment, "tag")
                .addToBackStack("tag")
                .commit();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main;
    }


    public static void launch(Context context) {
        Intent i = new Intent(context, CommunitySectionsActivity.class);
        context.startActivity(i);
    }
}
