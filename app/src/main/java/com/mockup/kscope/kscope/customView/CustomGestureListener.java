package com.mockup.kscope.kscope.customView;

import android.content.Context;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by Stafiiyevskyi on 28.10.2015.
 */
public class CustomGestureListener implements View.OnTouchListener {
    private GestureDetector detector;
    private SwipeGestureCallBack callBack;

    public CustomGestureListener(Context context) {
        detector = new GestureDetector(context, new MyGestureDetector());
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return detector.onTouchEvent(event);
    }

    class MyGestureDetector extends GestureDetector.SimpleOnGestureListener {
        private static final int SWIPE_MIN_DISTANCE = 20;
        private static final int SWIPE_THRESHOLD_VELOCITY = 100;

        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            if (Math.abs(e1.getX() - e2.getX()) > Math.abs(e1.getY() - e2.getY())) {
                if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE
                        && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                    callBack.onSwipe(Direction.LEFT);
                } else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE
                        && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                    callBack.onSwipe(Direction.RIGHT);
                }
            }

            return false;
        }

        @Override
        public boolean onDown(MotionEvent e) {

                return false;

        }

    }

    public void setSwipeGestureCallBack(SwipeGestureCallBack callBack) {
        this.callBack = callBack;
    }

    public interface SwipeGestureCallBack {
        public void onSwipe(Direction direction);
    }

    public static enum Direction{
        LEFT, RIGHT;
    }
}