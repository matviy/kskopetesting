package com.mockup.kscope.kscope.entity;

/**
 * Created by Alexander Rubanskiy on 12.06.2015.
 */

public class ItemCommunities {

    private String title;
    private String guid_community;
    private String logo;
    private String value;


    public ItemCommunities(String name, String value) {
        this.title = name;
        this.value = value;
    }

    public ItemCommunities(String name) {
        this.title = name;
    }


    public String getGuid_community() {
        return guid_community;
    }

    public void setGuid_community(String guid_community) {
        this.guid_community = guid_community;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }
}
