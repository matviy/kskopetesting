package com.mockup.kscope.kscope.fragments.main;

import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListView;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.adapters.AboutAdapter;
import com.mockup.kscope.kscope.entity.ItemAbout;
import com.mockup.kscope.kscope.fragments.BaseFragment;

import java.util.ArrayList;

import butterknife.InjectView;

/**
 * Created by Alexander Rubanskiy on 12.06.2015.
 */

public class FragmentAbout extends BaseFragment {

    @InjectView(R.id.list_about)
    ExpandableListView lvAbout;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        AboutAdapter adapter = new AboutAdapter(getActivity(), initDummyData());
        lvAbout.setAdapter(adapter);
        lvAbout.setGroupIndicator(null);
    }

    @Override
    public int getLayoutResource() {
        return R.layout.fragment_about;
    }

    private ArrayList<ItemAbout> initDummyData() {
        ArrayList<ItemAbout> about = new ArrayList<>();
        ItemAbout about1 = new ItemAbout("CPC", "Very very long text in this field. Very very long text in this field. Very very long text in this field. Very very long text in this field.");
        ItemAbout about2 = new ItemAbout("Testimonials", "And in this field we have very long text. And in this field we have very long text. And in this field we have very long text. And in this field we have very long text.");
        about.add(about1);
        about.add(about2);
        return about;
    }

}