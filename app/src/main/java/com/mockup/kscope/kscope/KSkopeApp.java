package com.mockup.kscope.kscope;

import android.app.Application;

import com.mockup.kscope.kscope.utils.PreferencesManager;

/**
 * Created by Serhii_Slobodianiuk on 02.11.2015.
 */
public class KSkopeApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        PreferencesManager.initializeInstance(this);
    }
}
