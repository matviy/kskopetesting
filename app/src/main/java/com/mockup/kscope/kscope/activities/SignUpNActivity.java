package com.mockup.kscope.kscope.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.api.SslRequestHandler;
import com.mockup.kscope.kscope.api.request.RegistrationRequest;
import com.mockup.kscope.kscope.api.response.RegistrationResponse;
import com.mockup.kscope.kscope.utils.PreferencesManager;
import com.mockup.kscope.kscope.utils.TypefaceManager;

import java.util.List;

import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by Stafiiyevskyi on 27.10.2015.
 */
public class SignUpNActivity extends BaseActivity implements Validator.ValidationListener{

    @NotEmpty
    @InjectView(R.id.email)
    EditText email;
    @NotEmpty
    @InjectView(R.id.password)
    EditText password;
    @InjectView(R.id.signup)
    Button signup;

    private Validator validator;
    private final PreferencesManager preferencesManager = PreferencesManager.getInstance();

    Handler h = new Handler();

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_signup;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        validator = new Validator(this);
        validator.setValidationListener(this);
        setTypefaceElements();
        email.setText("userlogin1");
        password.setText("password");
    }

    public static void launch(Context context) {
        Intent i = new Intent(context, SignUpNActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);
    }

    private void setTypefaceElements(){
        email.setTypeface(TypefaceManager.obtainTypeface(this, TypefaceManager.RALEWAY_REGULAR));
        password.setTypeface(TypefaceManager.obtainTypeface(this, TypefaceManager.RALEWAY_REGULAR));
        signup.setTypeface(TypefaceManager.obtainTypeface(this, TypefaceManager.RALEWAY_BOLD));
    }

    @Override
    public void onValidationSucceeded() {
        final RegistrationRequest request = new RegistrationRequest(email.getText().toString(), password.getText().toString());
        new Thread(new Runnable() {
            @Override
            public void run() {
                SslRequestHandler.getInstance().sendRequest(request, RegistrationResponse.class, new SslRequestHandler.SslRequestListener<RegistrationResponse>() {
                    @Override
                    public void onStart() {

                    }

                    @Override
                    public void onSuccess(final RegistrationResponse response) {
                        h.post(new Runnable() {
                            @Override
                            public void run() {
                                if (Integer.parseInt(response.getRequestResult()) == 0) {
                                    SignUpNActivity.this.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(SignUpNActivity.this, response.getRequestParam().getDetailErrorMessage(), Toast.LENGTH_LONG).show();
                                        }
                                    });
                                } else {
                                    preferencesManager.setUsername(response.getUserLogin());
                                    preferencesManager.setUserId(response.getIdUser());
                                    preferencesManager.setSessionId(response.getSessionId());
                                    MainActivity.launch(SignUpNActivity.this);
                                }
                                Log.e("SSLRequest", response.getIdUser() + " " + response.getUserLogin());
//                startActivity(new Intent(SignUpNActivity.this, SignupContainerActivity.class));
                            }
                        });

                    }

                    @Override
                    public void onFailure(String errorMsg) {

                    }

                    @Override
                    public void onFinish() {

                    }
                });
            }
        }).start();



    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);


            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @OnClick(R.id.signup)
    void onClickSignUp(){
        validator.validate();
    }
}
