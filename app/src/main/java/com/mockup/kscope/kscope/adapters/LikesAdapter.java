package com.mockup.kscope.kscope.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.entity.ItemLike;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Serhii_Slobodianiuk on 27.10.2015.
 */
public class LikesAdapter extends BaseAdapter {
    private List<ItemLike> likeList;
    private Context mContext;

    public LikesAdapter(Context mContext, List<ItemLike> likeList) {
        this.likeList = likeList;
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        return likeList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder holder;
        if (convertView == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.item_likes, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        Picasso.with(mContext)
                .load(likeList.get(position).getImage())
                .placeholder(R.drawable.bg_calendar_selected)
                .into(holder.image);

        holder.tvUsername.setText(likeList.get(position).getUsername());
        holder.tvLikes.setText(likeList.get(position).getLikes());
        holder.tvDate.setText(likeList.get(position).getDate());


        return view;
    }

    static class ViewHolder {

        @InjectView(R.id.username)
        TextView tvUsername;
        @InjectView(R.id.likes)
        TextView tvLikes;
        @InjectView(R.id.image)
        ImageView image;
        @InjectView(R.id.date)
        TextView tvDate;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }


}
