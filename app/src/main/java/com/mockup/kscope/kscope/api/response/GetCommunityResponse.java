package com.mockup.kscope.kscope.api.response;

import com.mockup.kscope.kscope.entity.ItemCommunities;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Stafiiyevskyi on 02.11.2015.
 */
public class GetCommunityResponse extends Response {

    private CommunityRequestParam request_param = new CommunityRequestParam();

    public List<ItemCommunities> getCommunity(){
        return request_param.community;
    }

    public CommunityRequestParam getRequestParam() {
        return request_param;
    }

    public static class CommunityRequestParam extends InvalidParam {

        private String data_count;
        private String data_limit;
        private String data_offset;
        private ArrayList<ItemCommunities> community;

        public String getDataCount() {
            return data_count;
        }

        public String getDataLimit() {
            return data_limit;
        }

        public String getDataOffset() {
            return data_offset;
        }

        public ArrayList<ItemCommunities> getCommunity(){
            return community;
        }
    }

}
