package com.mockup.kscope.kscope.utils;

import android.content.Context;

import com.mockup.kscope.kscope.R;

import org.apache.http.conn.ssl.SSLSocketFactory;

import java.io.InputStream;
import java.security.KeyStore;


/**
 * Created by mpodolsky on 04.11.2015.
 */
public class SSLUtils {

    public static SSLSocketFactory getSSLSocketFactory( Context c )
    {
        try
        {
            KeyStore trusted = KeyStore.getInstance( "BKS" );
            InputStream in = c.getResources().openRawResource( R.raw.debug );
            try
            {
                trusted.load( in, "android".toCharArray() );
            }
            finally
            {
                in.close();
            }
            return new SSLSocketFactory( trusted );
        }
        catch( Exception e )
        {
            throw new AssertionError( e );
        }
    }

}
