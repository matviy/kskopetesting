package com.mockup.kscope.kscope.entity;

import java.util.Date;

/**
 * Created by Stafiiyevskyi on 28.10.2015.
 */
public class ItemEvent {

    private String eventDescription;
    private Date dateOfEvent;
    private int typeOfPosition;

    public int getTypeOfPosition() {
        return typeOfPosition;
    }

    public void setTypeOfPosition(int typeOfPosition) {
        this.typeOfPosition = typeOfPosition;
    }

    public String getEventDescription() {
        return eventDescription;
    }

    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
    }

    public Date getDateOfEvent() {
        return dateOfEvent;
    }

    public void setDateOfEvent(Date dateOfEvent) {
        this.dateOfEvent = dateOfEvent;
    }
}
