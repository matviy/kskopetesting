package com.mockup.kscope.kscope.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Point;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

/**
 * Created by Stafiiyevskyi on 02.11.2015.
 */
public class DisplayUtil {

    private static Point screenSize;

    public static Point getScreenSize(Context context) {
        if (screenSize != null) {
            return screenSize;
        }
        int pxWidth;
        int pxHeight;

        Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        pxWidth = display.getWidth();
        pxHeight = display.getHeight();


        screenSize = new Point(pxWidth, pxHeight);
        return screenSize;
    }


    public static int getScreenWidth(Context context) {
        return getScreenSize(context).x;
    }

    public static int getScreenHeight(Context context) {
        return getScreenSize(context).y;
    }
}
