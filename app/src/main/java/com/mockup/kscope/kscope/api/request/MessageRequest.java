package com.mockup.kscope.kscope.api.request;

/**
 * Created by Serhii_Slobodianiuk on 03.11.2015.
 */
public class MessageRequest extends Request {

    private Message request_param;

    public MessageRequest(String sessionId) {
        setRequestCmd("get_message");
        setSessionId(sessionId);
    }

    public MessageRequest(String sessionId, String userId) {
        setRequestCmd("get_message");
        setSessionId(sessionId);
        request_param = new Message();
        request_param.id_user = userId;
    }

    private class Message {
        @SuppressWarnings("unused")
        private String id_user;
    }
}
