package com.mockup.kscope.kscope.fragments.tabs;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ListView;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.adapters.FriendsAdapter;
import com.mockup.kscope.kscope.adapters.MessagesAdapter;
import com.mockup.kscope.kscope.api.SslRequestHandler;
import com.mockup.kscope.kscope.api.request.MessageRequest;
import com.mockup.kscope.kscope.api.response.Response;
import com.mockup.kscope.kscope.entity.ItemFriend;
import com.mockup.kscope.kscope.entity.ItemMessage;
import com.mockup.kscope.kscope.fragments.BaseFragment;
import com.mockup.kscope.kscope.utils.PreferencesManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.InjectView;

/**
 * Created by Serhii_Slobodianiuk on 27.10.2015.
 */
public class FragmentMessages extends BaseFragment {

    @InjectView(R.id.list)
    ListView mList;

    private Context mContext;
    private PreferencesManager preferenceManager = PreferencesManager.getInstance();

    private List<ItemMessage> messagesList;
    private MessagesAdapter mAdapter;

    @Nullable
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        MessageRequest messageRequest = new MessageRequest(preferenceManager.getSessionId());
        SslRequestHandler.getInstance().sendRequest(messageRequest, Response.class, new SslRequestHandler.SslRequestListener<Response>() {
            @Override
            public void onStart() {

            }

            @Override
            public void onSuccess(Response response) {

            }

            @Override
            public void onFailure(String errorMsg) {

            }

            @Override
            public void onFinish() {

            }
        });


        messagesList = new ArrayList<>();

        for (int i = 0; i < 10; i++){
            ItemMessage message = new ItemMessage();
            message.setImage(null);
            message.setSender("Name Sender");
            message.setMessage("Very very long text in this field. Very very long text in this field. Very very long text in this field. Very very long text in this field.");
            message.setDate("4:54 PM");
            message.setOnline(((i % 2) == 0) ? true : false);
            messagesList.add(message);
        }

        mAdapter = new MessagesAdapter(mContext, messagesList);
        mList.setAdapter(mAdapter);
    }


    @Override
    public int getLayoutResource() {
        return R.layout.fragment_messages;
    }
}
