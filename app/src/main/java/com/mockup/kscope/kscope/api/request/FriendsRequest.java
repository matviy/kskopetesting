package com.mockup.kscope.kscope.api.request;

/**
 * Created by Serhii_Slobodianiuk on 02.11.2015.
 */
public class FriendsRequest extends Request {
    private Friends request_param;

    public FriendsRequest(String sessionId, String type) {
        setRequestCmd("get_contact");
        setSessionId(sessionId);
        request_param = new Friends();
        request_param.sort_type = type;
    }

    public FriendsRequest(String sessionId) {
        setRequestCmd("get_contact");
        setSessionId(sessionId);
    }

    private class Friends {
        @SuppressWarnings("unused")
        private String sort_type = "2";
    }
}
