package com.mockup.kscope.kscope.api.request;



/**
 * Created by Stafiiyevskyi on 02.11.2015.
 */
public class GetSponsorRequest extends Request {

    private GetSponsor request_param;

    public GetSponsorRequest(String guidSponsor){
        setRequestCmd("get_sponsor");
        request_param = new GetSponsor();
        request_param.guid_sponsor = guidSponsor;
    }

    public GetSponsorRequest(){
        setRequestCmd("get_sponsor");
    }

    private class GetSponsor {
        @SuppressWarnings("unused")
        private String guid_sponsor;
    }

}
