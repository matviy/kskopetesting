package com.mockup.kscope.kscope.api.request;

/**
 * Created by Stafiiyevskyi on 02.11.2015.
 */
public class GetCommunityRequest extends Request {

    private GetCommunity request_param;

    public GetCommunityRequest(String guidCommunity){
        setRequestCmd("get_community");
        request_param = new GetCommunity();
        request_param.guid_community = guidCommunity;
    }

    public GetCommunityRequest(){
        setRequestCmd("get_community");
    }

    private class GetCommunity {
        @SuppressWarnings("unused")
        private String guid_community;
    }

}