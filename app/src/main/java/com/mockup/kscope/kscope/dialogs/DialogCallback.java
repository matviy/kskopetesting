package com.mockup.kscope.kscope.dialogs;

public interface DialogCallback {

    void confirm();

    void cancel();

    public static class EmptyCallback implements DialogCallback {

        @Override
        public void confirm() {

        }

        @Override
        public void cancel() {

        }
    }

}