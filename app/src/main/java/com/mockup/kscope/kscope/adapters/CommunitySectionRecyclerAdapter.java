package com.mockup.kscope.kscope.adapters;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.activities.CommunitySectionsActivity;
import com.mockup.kscope.kscope.adapters.viewHolder.ViewHolderCommunitySection;
import com.mockup.kscope.kscope.entity.ItemCommunities;
import com.mockup.kscope.kscope.fragments.community_sections.FragmentCommunitySubSection;

import java.util.List;

/**
 * Created by Stafiiyevskyi on 29.10.2015.
 */
public class CommunitySectionRecyclerAdapter extends RecyclerView.Adapter<ViewHolderCommunitySection> {

    private Activity context;
    private List<ItemCommunities> data;
    private CallBackClickItem recyclerView;

    public CommunitySectionRecyclerAdapter(Activity context, CallBackClickItem recyclerView) {
        this.context = context;
        this.recyclerView = recyclerView;
    }


    @Override
    public ViewHolderCommunitySection onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_community_sections, parent, false);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerView.onItemRecyclerClick(v);
            }
        });
        ViewHolderCommunitySection recyclerViewHolder = new ViewHolderCommunitySection(view);
        return recyclerViewHolder;

    }


    @Override
    public void onBindViewHolder(final ViewHolderCommunitySection holder, final int position) {

        holder.nameCommunity.setText(data.get(position).getName());
        holder.bannerCommunity.setImageDrawable(context.getResources()
                .getDrawable(R.drawable.kscope_logo));
    }

    @Override
    public int getItemCount() {
        return data.size();

    }


    public void onCommunityClick(int position) {
        Fragment fragment = new FragmentCommunitySubSection();
        Bundle bundle = new Bundle();
        bundle.putString(CommunitySectionsActivity.TITLE_KEY, data.get(position).getName());
        fragment.setArguments(bundle);
        ((CommunitySectionsActivity) context).addFragment(fragment);

    }


    public void setData(List<ItemCommunities> data) {
        this.data = data;
    }

    public interface CallBackClickItem{
        void onItemRecyclerClick(View v);
    }
}
