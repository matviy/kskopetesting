package com.mockup.kscope.kscope.fragments.community_sections;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.activities.CommunitySectionsActivity;
import com.mockup.kscope.kscope.adapters.CommunitySectionRecyclerAdapter;
import com.mockup.kscope.kscope.api.SslRequestHandler;
import com.mockup.kscope.kscope.api.request.GetCommunityRequest;
import com.mockup.kscope.kscope.api.response.GetCommunityResponse;
import com.mockup.kscope.kscope.api.response.Response;
import com.mockup.kscope.kscope.customView.CalendarItemDecoration;
import com.mockup.kscope.kscope.entity.ItemCommunities;
import com.mockup.kscope.kscope.fragments.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.InjectView;

/**
 * Created by Stafiiyevskyi on 29.10.2015.
 */
public class FragmentCommunitySectionRecycler extends BaseFragment implements CommunitySectionRecyclerAdapter.CallBackClickItem{
    @InjectView(R.id.communities_sections_recycler)
    RecyclerView recyclerView;
    @InjectView(R.id.progress)
    ProgressBar progressBar;
    @InjectView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    private CommunitySectionRecyclerAdapter adapter;

    SslRequestHandler.SslRequestListener requestListener;
    @Override
    public int getLayoutResource() {
        return R.layout.fragment_community_sections;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        StaggeredGridLayoutManager layoutManager = new /*GridLayoutManager(getActivity(), 2);//*/StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new CommunitySectionRecyclerAdapter(getActivity(),this);
        List<ItemCommunities> list = new ArrayList<>();
        prepareRecyclerView(list);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                getCommunities();
            }
        });

                requestListener = new SslRequestHandler.SslRequestListener<GetCommunityResponse>() {
            List<ItemCommunities> communities;

            @Override
            public void onStart() {
                if (!swipeRefreshLayout.isRefreshing())
                    progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onSuccess(GetCommunityResponse response) {
                communities = response.getCommunity();
            }

            @Override
            public void onFailure(String errorMsg) {

            }

            @Override
            public void onFinish() {
                progressBar.setVisibility(View.INVISIBLE);
                swipeRefreshLayout.setRefreshing(false);
                FragmentCommunitySectionRecycler.this.prepareRecyclerView(communities);
            }
        };

        getCommunities();

        return view;
    }



    private void prepareRecyclerView(List<ItemCommunities> communities){
        adapter = new CommunitySectionRecyclerAdapter(getActivity(),this);
        adapter.setData(communities);
        recyclerView.setAdapter(adapter);

    }


    private void getCommunities(){
        GetCommunityRequest getCommunityRequest = new GetCommunityRequest();
        SslRequestHandler.getInstance().sendRequest(getCommunityRequest, GetCommunityResponse.class, requestListener);
    }



    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        ((CommunitySectionsActivity)getActivity()).setActionBarTitle("Community Sections",true);
    }

    @Override
    public void onItemRecyclerClick(View v) {
        int position = recyclerView.getChildAdapterPosition(v);
        adapter.onCommunityClick(position);
    }


    @Override
    public void onPause() {
        super.onPause();
        SslRequestHandler.getInstance().cancelAsyncRequest(requestListener);
    }
}