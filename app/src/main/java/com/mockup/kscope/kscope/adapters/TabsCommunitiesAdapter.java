package com.mockup.kscope.kscope.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.entity.ItemCommunities;
import com.mockup.kscope.kscope.entity.ItemFriend;
import com.squareup.picasso.Picasso;

import java.util.List;

public class TabsCommunitiesAdapter extends BaseExpandableListAdapter {

    public static final int GROUP_ITEM_LAYOUT = R.layout.item_communities_group;
    public static final int CHILD_ITEM_LAYOUT = R.layout.item_communities_child;

    private List<ItemCommunities> groups;
    private Context context;

    public TabsCommunitiesAdapter(Context context, List<ItemCommunities> groups) {
        this.context = context;
        this.groups = groups;
    }
    public void setItems(List<ItemCommunities> groups) {
        this.groups = groups;
        notifyDataSetChanged();
    }

    @Override
    public int getGroupCount() {
        return groups.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groups.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return groups.get(groupPosition).getValue();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView,
                             ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(GROUP_ITEM_LAYOUT, null);
        }

        ImageView ivArrow = (ImageView) convertView.findViewById(R.id.iv_group_about_arrow);
        ImageView ivLogo = (ImageView) convertView.findViewById(R.id.communities_logo);
        Picasso.with(context)
                .load(groups.get(groupPosition).getLogo())
                .noPlaceholder()
                .into(ivLogo);
        if (isExpanded) {
            ivArrow.setImageResource(R.drawable.ic_arrow_light_selected);
        } else {
            ivArrow.setImageResource(R.drawable.ic_arrow_light_normal);
        }

        TextView tvGroupName = (TextView) convertView.findViewById(R.id.tv_community_group_name);
        tvGroupName.setText(groups.get(groupPosition).getName());

        return convertView;

    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild,
                             View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(CHILD_ITEM_LAYOUT, null);
        }

        TextView tvChildName = (TextView) convertView.findViewById(R.id.tv_communities_child_text);
        tvChildName.setText(groups.get(groupPosition).getValue());

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
