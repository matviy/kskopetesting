package com.mockup.kscope.kscope.adapters.viewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mockup.kscope.kscope.R;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Stafiiyevskyi on 28.10.2015.
 */
public class SponsorsRecyclerViewHolder extends RecyclerView.ViewHolder {

    @InjectView(R.id.sponsor_image)
    public ImageView sponsorBanner;
    @InjectView(R.id.sponsor_textview)
    public TextView sponsorDescription;


    public SponsorsRecyclerViewHolder(View itemView) {
        super(itemView);
        ButterKnife.inject(this, itemView);
    }
}
