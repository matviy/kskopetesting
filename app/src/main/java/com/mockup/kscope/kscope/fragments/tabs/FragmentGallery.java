package com.mockup.kscope.kscope.fragments.tabs;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.adapters.GalleryAdapter;
import com.mockup.kscope.kscope.customView.CustomItemDecoration;
import com.mockup.kscope.kscope.entity.ItemGallery;
import com.mockup.kscope.kscope.fragments.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.InjectView;

/**
 * Created by Serhii_Slobodianiuk on 27.10.2015.
 */
@SuppressWarnings("ALL")
public class FragmentGallery extends BaseFragment {

    @InjectView(R.id.gallery)
    RecyclerView mGalleryGrid;
    private GalleryAdapter mAdapter;
    //private StaggeredGridLayoutManager mLayoutManager;
    private GridLayoutManager mLayoutManager;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initGrid();
        loadItems();
    }

    private void loadItems() {
        List<ItemGallery> assets = new ArrayList<>();
        for (int i = 0; i < 20; i++){
            ItemGallery itemGallery = new ItemGallery();
            itemGallery.setPhoto(R.drawable.rectangle);
            itemGallery.setType(((i%2)==0) ? "videos" : "images");
            assets.add(itemGallery);
        }
        mAdapter.addItems(assets);
    }

    private void initGrid() {
        mAdapter = new GalleryAdapter(getActivity(), new ArrayList<ItemGallery>());
        //callback mAdapter.setCallback
        mGalleryGrid.setHasFixedSize(true);

//        mLayoutManager = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
//        mLayoutManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_MOVE_ITEMS_BETWEEN_SPANS);
        mLayoutManager = new GridLayoutManager(getActivity(), 3);
        mGalleryGrid.setLayoutManager(mLayoutManager);
        mGalleryGrid.setBackgroundResource(R.color.more_light_gray);
        mGalleryGrid.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                int visibleItemCount = mLayoutManager.getChildCount();
                int totalItemCount = mLayoutManager.getItemCount();
                int[] firstVisibleItems = new int[4];
                int[] lastVisibleItems = new int[4];
//                firstVisibleItems = mLayoutManager.findFirstVisibleItemPositions(firstVisibleItems);
//                lastVisibleItems = mLayoutManager.findLastVisibleItemPositions(lastVisibleItems);
                int firstVisiblePosition = mLayoutManager.findFirstVisibleItemPosition();
                int lastVisiblePosition = mLayoutManager.findLastVisibleItemPosition();
                //load new item
//                if (!loading) {
////                    if ((visibleItemCount + firstVisibleItems[0]) >= totalItemCount) {
//                    if (lastVisibleItems[0] >= mCurrentCount - ITEM_LOAD_COUNT && mCurrentCount > 0) {
//                        loading = true;
//                        Log.d("RESTCOUNTER", "from scroll listener");
//                        loadMoreItems();
//                    }
//                }
                //mAdapter.scroll(firstVisibleItems[0], lastVisibleItems[0]);
                mAdapter.scroll(firstVisiblePosition, lastVisiblePosition);
            }
        });

        mGalleryGrid.addItemDecoration(new CustomItemDecoration(
                getResources().getDimensionPixelSize(R.dimen.gallery_padding)
        ));
        mGalleryGrid.setAdapter(mAdapter);
    }
    @Override
    public int getLayoutResource() {
        return R.layout.fragment_gallery;
    }
}
