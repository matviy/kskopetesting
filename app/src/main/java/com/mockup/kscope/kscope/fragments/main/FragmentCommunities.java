package com.mockup.kscope.kscope.fragments.main;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.adapters.CommunitiesAdapter;
import com.mockup.kscope.kscope.entity.ItemCommunities;
import com.mockup.kscope.kscope.fragments.BaseFragment;

import java.util.ArrayList;

import butterknife.InjectView;

/**
 * Created by Alexander Rubanskiy on 12.06.2015.
 */

public class FragmentCommunities extends BaseFragment {

    @InjectView(R.id.communities_list)
    ListView lvCommunities;

    private String[] ArrayCommunities = {"African - American", "Asian - Pacific Rim",
            "Caribbean - West Indian", "Hispanic - Latino - Portuge",
            "Native American", "Middle Eastern, Indian"};

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        CommunitiesAdapter adapter = new CommunitiesAdapter(getActivity(), initList());
        lvCommunities.setAdapter(adapter);
    }

    private ArrayList<ItemCommunities> initList() {
        ArrayList<ItemCommunities> items = new ArrayList<>();
        for (int i = 0; i < ArrayCommunities.length; i++) {
            ItemCommunities itemMainMenu = new ItemCommunities(ArrayCommunities[i]);
            items.add(itemMainMenu);
        }
        return items;
    }

    @Override
    public int getLayoutResource() {
        return R.layout.fragment_communities;
    }

}