package com.mockup.kscope.kscope.activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.utils.TypefaceManager;

import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by Stafiiyevskyi on 26.10.2015.
 */

public class StartUpActivity extends BaseActivity {
    @InjectView(R.id.signup)
    Button signUp;
    @InjectView(R.id.signin)
    Button signIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTypefaceElements();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_startup;
    }

    private void setTypefaceElements(){
        signUp.setTypeface(TypefaceManager.obtainTypeface(this,TypefaceManager.RALEWAY_BOLD));
        signIn.setTypeface(TypefaceManager.obtainTypeface(this,TypefaceManager.RALEWAY_BOLD));
    }

    @OnClick(R.id.signup)
    void onClickSignUp(){
        startActivity(new Intent(StartUpActivity.this, SignUpNActivity.class));
    }

    @OnClick(R.id.signin)
    void onClickSignIn(){
        startActivity(new Intent(StartUpActivity.this, SignInActivity.class));
    }
}
