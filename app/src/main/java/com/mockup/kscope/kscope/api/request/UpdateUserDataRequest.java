package com.mockup.kscope.kscope.api.request;

import com.mockup.kscope.kscope.api.model.User;

import java.util.ArrayList;

public class UpdateUserDataRequest extends Request {

    private UpdateUser request_param;
    public UpdateUserDataRequest(String sessionId, ArrayList<User> user) {
        setRequestCmd("update_user_data");
        setSessionId(sessionId);
        request_param = new UpdateUser();
        request_param.user_data = user;
    }

    private class UpdateUser{
        @SuppressWarnings("unused")
        ArrayList<User> user_data;
    }
}
