package com.mockup.kscope.kscope.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.fragments.main.FragmentMainMenu;

public class MainActivity extends BaseActivity {

    public static void launch(Context context) {
        Intent i = new Intent(context, MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Fragment fragment = new FragmentMainMenu();
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.fragmants_container, fragment, "tag")
                .commit();
        setActionBarTitle("Main menu", true);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main;
    }

    public void addFragment(Fragment fragment, String title) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.fragmants_container, fragment, "tag")
                .addToBackStack("tag")
                .commit();
        setActionBarTitle(title, true);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setActionBarTitle("Main menu", true);
    }
}
