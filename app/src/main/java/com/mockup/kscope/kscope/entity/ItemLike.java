package com.mockup.kscope.kscope.entity;

/**
 * Created by Serhii_Slobodianiuk on 28.10.2015.
 */
public class ItemLike {
    private String image;
    private String username;
    private String likes;
    private String date;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
