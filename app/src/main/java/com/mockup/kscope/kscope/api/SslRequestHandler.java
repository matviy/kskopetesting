package com.mockup.kscope.kscope.api;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mockup.kscope.kscope.api.request.Request;
import com.mockup.kscope.kscope.api.response.Response;
import com.mockup.kscope.kscope.ssl.MySSLSocketFactory;

import org.apache.http.client.HttpClient;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.charset.Charset;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * Created by Alexander Rubanskiy on 30.10.2015.
 */

public class SslRequestHandler {

    public static int SERVER_PORT = 5252;
    public static String SERVER_IP = "52.25.51.200";

    protected static final String TAG_RESPONSE = "Response";
    protected static final String TAG_REQUEST = "Request";

    protected static final String REQUEST_PROTOCOL = "TLS";
    protected static final String DEFAULT_CHARSET = "utf-8";

    private ExecutorService executor = Executors.newSingleThreadExecutor();

    private final Handler handler = new Handler(Looper.getMainLooper());

    private List<SslRequestListener<?>> callbacks = new CopyOnWriteArrayList<>();

    /**
     * Interface definition for a callback to be invoked when request
     * state was changed
     */
    public interface SslRequestListener <T> {
        /**
         * Callback method to be invoked when request has been started
         * Must be called on UI thread.
         */
        void onStart();
        /**
         * Called when request is successful
         *
         * @param response the response
         */
        void onSuccess(T response);
        /**
         * Called when request is failed
         *
         * @param errorMsg error message
         */
        void onFailure(String errorMsg);
        /**
         * Callback method to be invoked when request has been finished
         * Must be called on UI thread.
         */
        void onFinish();
    }

    private SslRequestHandler() {
    }

    private static class Holder {
        private static final SslRequestHandler INSTANCE = new SslRequestHandler();
    }

    public static SslRequestHandler getInstance() {
        return Holder.INSTANCE;
    }

    /**
     * Send request asynchronously
     *
     * @param request
     *            the request.
     * @param callback callback to be invoked when request state was changed
     * @param clazz class of response
     * @throws IllegalStateException
     *             when {@code request == null}
     */
    public synchronized <T extends Response> void sendRequest(final Request request, final Class<T> clazz,
                                                              final SslRequestListener<T> callback) {

        if(request == null) {
            throw new IllegalStateException("Request must not be null.");
        }

        callbacks.add(callback);

        handler.post(new Runnable() {
            @Override
            public void run() {
                callback.onStart();
            }
        });

        Runnable task = new Runnable() {
            @Override
            public void run() {
                if(!callbacks.contains(callback)) {
                    return;
                }

                T responseObj = sendRequestSync(request, clazz);
                if(responseObj != null) {
                    callback.onSuccess(responseObj);
                } else {
                    if(callback != null) {
                        callback.onFailure("Sorry, no connection to server. Please try again later.");
                    }
                }

                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if(callback != null) {
                            callback.onFinish();
                        }
                    }
                });

                callbacks.remove(callback);
            }
        };

        executor.submit(task);
    }

    /**
     * Send request synchronously
     *
     * @param request
     *            the request.
     * @param clazz
     *            class of response
     * @return the response
     * @throws IllegalStateException
     *             when {@code request == null}
     */
    public synchronized <T> T sendRequestSync(Request request, Class<T> clazz) {

        if(request == null) {
            throw new IllegalStateException("Request must not be null.");
        }

        Socket socket = null;
        OutputStream outputStream = null;

        Gson gson = new GsonBuilder().create();

        try {
//            KeyStore trustStore = KeyStore.getInstance(KeyStore
//                    .getDefaultType());
//            trustStore.load(null, null);

            SSLContext sslContext = SSLContext.getInstance(REQUEST_PROTOCOL);
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();
//            MySSLSocketFactory sslSocketFactory = new MySSLSocketFactory(sslContext);
            socket = sslSocketFactory.createSocket(SERVER_IP, SERVER_PORT);
            socket.setSoTimeout(10 * 1000);
            outputStream = socket.getOutputStream();
            byte[] postData = gson.toJson(request).getBytes();
            outputStream.write(postData);

            Log.v(TAG_REQUEST, gson.toJson(request));

            InputStream inputStream = socket.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream, Charset.forName(DEFAULT_CHARSET)));

            StringBuilder response = new StringBuilder();
            String tmp;

            while ((tmp = br.readLine()) != null) {
                Log.v(TAG_RESPONSE, tmp);
                response.append(tmp);
            }

            T responseObj = gson.fromJson(response.toString(), clazz);

            return responseObj;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if(outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                }
            }
            if(socket != null) {
                try {
                    socket.close();
                } catch (IOException e) {
                }
            }
        }
    }

    public synchronized void cancelAsyncRequest(SslRequestListener<?> callback) {
        callbacks.remove(callback);
    }

    public synchronized void cancelAllAsyncRequest() {
        callbacks.clear();
    }

    private final TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
            return null;
        }

        public void checkClientTrusted(
                java.security.cert.X509Certificate[] certs, String authType) {
        }

        public void checkServerTrusted(
                java.security.cert.X509Certificate[] certs, String authType) {
        }
    } };

    private final HttpClient sslClient(HttpClient client) {
        try {
            X509TrustManager tm = new X509TrustManager() {
                public void checkClientTrusted(X509Certificate[] xcs, String string) throws CertificateException {
                }

                public void checkServerTrusted(X509Certificate[] xcs, String string) throws CertificateException {
                }

                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
            };
            SSLContext ctx = SSLContext.getInstance("TLS");
            ctx.init(null, new TrustManager[]{tm}, null);
            org.apache.http.conn.ssl.SSLSocketFactory ssf = new MySSLSocketFactory(ctx);
            ssf.setHostnameVerifier(org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            ClientConnectionManager ccm = client.getConnectionManager();
            SchemeRegistry sr = ccm.getSchemeRegistry();
            sr.register(new Scheme("https", ssf, 443));
            return new DefaultHttpClient(ccm, client.getParams());
        } catch (Exception ex) {
            return null;
        }
    }

}
