package com.mockup.kscope.kscope.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.entity.ItemFriend;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Serhii_Slobodianiuk on 27.10.2015.
 */
public class FriendsAdapter extends BaseAdapter {
    private List<ItemFriend> friendsList;
    private Context mContext;

    public FriendsAdapter(Context mContext, List<ItemFriend> friendsList) {
        this.friendsList = friendsList;
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        return friendsList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setItems(List<ItemFriend> friends) {
        this.friendsList = friends;
        notifyDataSetChanged();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder holder;
        if (convertView == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.item_friends, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        Picasso.with(mContext)
                .load(R.drawable.ic_photo_placeholder)
                //.load(friendsList.get(position).getPhoto())
                .placeholder(R.drawable.ic_photo_placeholder)
                .into(holder.ivFriendsPhoto);

        holder.tvFriendsName.setText(friendsList.get(position).getUser_login());
        holder.tvFriendsFollowers.setText("123" + " Following");
        //holder.tvFriendsFollowers.setText(friendsList.get(position).getFollowers() + " Following");

        return view;
    }

    static class ViewHolder {

        @InjectView(R.id.friends_name)
        TextView tvFriendsName;
        @InjectView(R.id.friends_followers)
        TextView tvFriendsFollowers;
        @InjectView(R.id.image)
        ImageView ivFriendsPhoto;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }


}
