package com.mockup.kscope.kscope.fragments.main;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.EditText;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.api.SslRequestHandler;
import com.mockup.kscope.kscope.api.model.User;
import com.mockup.kscope.kscope.api.request.UpdateUserDataRequest;
import com.mockup.kscope.kscope.api.request.UserDataRequest;
import com.mockup.kscope.kscope.api.response.Response;
import com.mockup.kscope.kscope.api.response.UserDataResponse;
import com.mockup.kscope.kscope.fragments.BaseFragment;
import com.mockup.kscope.kscope.utils.PreferencesManager;

import java.util.ArrayList;

import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by Serhii_Slobodianiuk on 02.11.2015.
 */
public class FragmentEditProfile extends BaseFragment {

    @InjectView(R.id.first_name)
    EditText etFirstName;
    @InjectView(R.id.last_name)
    EditText etLastName;
    @InjectView(R.id.info)
    EditText etInfo;
    @InjectView(R.id.address)
    EditText etAddress;
    private PreferencesManager preferenceManager = PreferencesManager.getInstance();

    public static Fragment newInstance() {
        FragmentEditProfile fragmentEditProfile = new FragmentEditProfile();
        Bundle args = new Bundle();
        fragmentEditProfile.setArguments(args);
        return fragmentEditProfile;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @OnClick(R.id.confirm)
    void onConfirmUpdate(){
        String firstName = etFirstName.getText().toString();
        String lastName = etLastName.getText().toString();
        String info = etInfo.getText().toString();
        String address = etAddress.getText().toString();

        User user = new User(firstName, lastName, info, address);
        ArrayList<User> userList = new ArrayList<>();
        userList.add(user);
        UpdateUserDataRequest updateUserDataRequest = new UpdateUserDataRequest(preferenceManager.getSessionId(), userList);
        SslRequestHandler.getInstance().sendRequest(updateUserDataRequest, Response.class, new SslRequestHandler.SslRequestListener<Response>() {
            @Override
            public void onStart() {

            }

            @Override
            public void onSuccess(Response response) {

                getActivity().getSupportFragmentManager().popBackStack();
            }

            @Override
            public void onFailure(String errorMsg) {

            }

            @Override
            public void onFinish() {

            }
        });
    }

    @Override
    public int getLayoutResource() {
        return R.layout.fragment_edit_profile;
    }
}
